var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: '#eval-source-map',
  resolve: {
    root: [
      path.resolve('src')
    ],
    fallback: [path.join(__dirname, 'node_modules')],
  },
  entry: {
    app: [
      './src/index'
    ],
    vendor: [
      'lodash',
      'jquery',
      'react',
      'react-dom',
      'react-router',
      'react-weui'
    ]
  },
  output: {
    path: path.join(__dirname, 'wechat_119'),
    filename: '[name].js',
    publicPath: '/wechat_119/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery',
      '_': 'lodash'
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /zh-cn/),
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['react-hot', 'babel'],
      include: path.join(__dirname, 'src')
    }, {
      test: /\.css$/,
      loaders: ['style', 'css']
    }, {
      test: /\.(woff2?|eot|ttf|svg)(\?.*)?$/,
      loader: 'file',
      query: {
        limit: 10000,
        name: 'font/[name]_[hash:8].[ext]'
      }
    }]
  }
};
