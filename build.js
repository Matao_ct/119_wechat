require('shelljs/global')
env.NODE_ENV = 'production'

var path = require('path')
var webpack = require('webpack')
var webpackConfig = require('./webpack.prod.conf')

//static
// var staticPath = path.resolve(__dirname, 'wechat/static')
// cp('-R', 'wechat/static', staticPath)

var copy_dist = function () {
  var publicPath = path.resolve(__dirname, '../fire_fighting_web/src/public')

  console.log('copying dist to', publicPath)

  cp('-R', 'wechat_119/', publicPath)

  console.log('done', publicPath)
}

webpack(webpackConfig, function (err, stats) {
  if (err) throw err
  console.log(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
  }))
  copy_dist()
})
