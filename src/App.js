import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


import Page from './components/page';


class App extends Component {
  render() {
    return (
      <Page title="查询">
        <div className="App">
          <div className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h2>Welcome to React</h2>
          </div>
        </div>
      </Page>
    );
  }
}

export default App;
