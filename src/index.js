import React from 'react';
import ReactDOM from 'react-dom';

import CustomQuery from './Pages/CustomQuery';
import HomePage from './Pages/TabPage';
import CustomGuide from './Pages/CustomGuide';
import TabPage from './Pages/TabPage';
import Complaints from './Pages/Complaints';
import Advice from './Pages/Advice';
import ReportStepOne from './Pages/ReportStepOne';
import ReportStepTwo from './Pages/ReportStepTwo';
import ReportStepThree from './Pages/ReportStepThree';
import CustomQueryResult from './Pages/CustomQueryResult';
import GuideContent from './Pages/GuideContent';
import OperationOLList from './Pages/OperationOLList';
import OperationOLUploader from './Pages/OperationOLUploader';
import SceneGuide from './Pages/SceneGuide';

import Assistor from './Pages/Assistor';
import FilesUploader from './Pages/FilesUploader'

import './index.css';

import {Router, Route, hashHistory,browserHistory} from 'react-router';



ReactDOM.render((
        <Router history={hashHistory}>
          <Route path="/" component={HomePage} />
          <Route path="/tabPage" component={TabPage}/>
          <Route path="/home" component={HomePage} />
          <Route path="/customQuery" component={CustomQuery}/>
          <Route path="/customGuide" component={CustomGuide}/>
          <Route path="/complaints" component={Complaints}/>
          <Route path="/advice" component={Advice}/>
          <Route path="/reportStep1" component={ReportStepOne}/>
          <Route path="/reportStep2" component={ReportStepTwo}/>
          <Route path="/reportStep3" component={ReportStepThree}/>
          <Route path="/queryResult" component={CustomQueryResult}/>
          <Route path="/guideContent" component={GuideContent}/>
          <Route path="/operationOLList" component={OperationOLList}/>
          <Route path="/operationOLUploader" component={OperationOLUploader}/>
          <Route path="/sceneGuide" component={SceneGuide}/>
          <Route path="/assistor" component={Assistor}/>
          <Route path="/fileUploader" component={FilesUploader}/>
        </Router>
    ),
  document.getElementById('root')
);
