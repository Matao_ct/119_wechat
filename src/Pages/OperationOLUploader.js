import React, {Component} from 'react'
import Page from '../components/page'

import {
    ButtonArea,
    Button,
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    MediaBoxInfo,
    MediaBoxInfoMeta,
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    CellsTitle,
    Form,
    FormCell,
    Label,
    Input,
    Select,
    Uploader,
    TextArea,
    Toast,
    Dialog,
    Msg,
} from 'react-weui';
const {Alert} = Dialog;
var Config = require('./Config');
var Ajax = require('../components/ajax');
import WxUploader from '../components/WxImageComponent'
import {AudioUploader} from '../components/AudioUploader'

import {const_parse} from '../components/util';


import {hashHistory} from 'react-router';

var serverUrls = [];

var project_types = {
  'NEW': '新建',
  'EXTEND': '扩建',
  'DECORATE': '装修',
  'KEEP_TEMP': '建筑保温',
  'CHANGE_USAGE': '改变用途'
}

var usage_type_for_base_info = {
  'A1': '公共娱乐场所',
  'A2': '其他公众聚集场所',
  'A3': '其他人员密集场所',
  'A4': '易燃易爆场所',
  'A5': '二类高层住宅',
  'A6': '其它高层建筑',
  'A7': '地下、半地下建筑',
  'A8': '火灾危险性为丙、丁、戊类的工业建筑',
  'A9': '其它建设工程',
  'A10': '新法实施前已经消防审核的建设工程',
  'A11': '依照法规应予抽查的建设工程',
}

export default class OperationOLUploader extends Component {
  constructor(props) {
    super(props);
    this._initialState();
  }


  _initialState() {



    var area1 = this._makeArea1();


    var area2 = [];
    if(this.props.location.state.area1 != undefined)
    {
      area2 = this.props.location.state.area2Source;
    }
    else{
      area2 = this._makeArea2(area1[0].value);
    }
    if(this.props.location.state.area1 != undefined)
    {
      this.state = this.props.location.state;

      console.log(this.state);
      return;
    }


    this.state = {


      company:this.props.location.state.company,
      id:this.props.location.state.id,


      //基本信息
      area1:area1[0].value,
      area2:area2[0].value,
      area3:'',
      area1Source:area1,
      area2Source:area2,
      area3Source:[],
      projectName:'',
      projectAddress:'',
      projectBuilder:'',
      projectBoss:'',
      projectBossTel:'',
      contact:'',
      contactTel:'',
      startTime:'',
      endTime:'',
      type:'NEW',
      useCase:'A1',
      investment:'',
      licenceId:'',


      //协同单位
        designername:'',
        designerlevel:'',
        designerboss:'',
        designerbossTel:'',
        designercontact:'',
        operatorname:'',
        operatorlevel:'',
        operatorboss:'',
        operatorbossTel:'',
        operatorcontact:'',
        monitorname:'',
        monitorlevel:'',
        monitorboss:'',
        monitorbossTel:'',
        monitorcontact:'',

      dataSource: {
        sheets:[],
        businessLicense:[],
      },
      item:this.props.location.state,


      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },

      //当前保存状态
      isSaved: true,

      //校验提示
      alert: {
        showAlert: false,
        title: '标题标题',
        buttons: [
          {
            label: '好的',
            onClick: this._hideAlert.bind(this)
          }
        ]
      },

      //提交成功后的信息
      backInfo:{
        deadLine:'',
        code:''
      },

      //提交成功的判断
      success:false,
    }
  }


  componentDidMount() {
    this.context.router.setRouteLeaveHook(
        this.props.route,
        this.routerWillLeave.bind(this)
    )

    this._requestWindowId(this.state.area2);
  }

  routerWillLeave(nextLocation) {
    // 返回 false 会继续停留当前页面，
    // 否则，返回一个字符串，会显示给用户，让其自己决定
    if (!this.state.isSaved)
      return '改动未提交,确认要离开？';
  }

  _showToast(msg, icon, timer,callback) {
    this.setState({
      showToast: {
        show: true,
        icon: icon,
        msg: msg,
      },
    });
    console.log("timer",timer);
    console.log("callback",callback);

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _showAlert(title, buttonLabel) {
    this.setState({
      alert: {
        showAlert: true,
        title: title,
        buttons: [{label: buttonLabel, onClick: this._hideAlert.bind(this)}]
      }
    });
  }

  _hideAlert() {
    this.setState({alert: {showAlert: false, title: '', buttons: [{label: '', onClick: this._hideAlert.bind(this)}]}});
  }

  _changeImageFile(newFiles) {
    this.state.isSaved = false;
    console.log("sheetFiles " , newFiles);
    this.state.dataSource.sheets = newFiles;
  }

  _changeBusinessFile(newFiles) {
    this.state.isSaved = false;
    console.log("businessLicense " , newFiles);
    this.state.dataSource.businessLicense = newFiles;
  }

  _goBack() {
    this.props.history.pushState(this.props.location.state, `/`, null);
  }

  _makeArea1(){
    var area1 = [];
    _.map(Config.area,(value,key) =>{
      var item = {};
      item['value'] = value.id;
      item['label'] = value.name;
      area1.push(item);
    });
    console.log('area',area1);
    return area1;
  }

  _makeArea2(area1){

    console.log('area1 select',area1);

    var source = _.filter(Config.area,(value,key)=>{
      if( value.id == area1)
        return value;
    })

    var area2 = [];

    _.map(source[0].children,(value,key)=>{
      var item ={};
      item['value'] = value.id;
      item['label'] = value.name;

      area2.push(item);
    })
    return area2;
  }

  _makeArea3(json){
    var area3 = [];
    _.map(json,(value,key)=>{
      var item = {};
      item['value'] = value.id;
      item['label'] = value.name;
      area3.push(item);
    })
    return area3;
  }

  _requestWindowId(regionId){
    Ajax.GET(this.props.window, {data: {regionId:regionId}})
    .then((res) => {

      console.log('area3',res);

      var source = this._makeArea3(res);

      this.setState({area3Source:source,area3:source[0].value})

    })
    .catch((res) => {

    })

  }


  _onProjectName(event){
    let selected = event.target.value;

    console.log('name',selected);
    this.setState({projectName:selected});


  }

  _onProjectAddress(event){
    let selected = event.target.value;
    this.setState({projectAddress:selected});
  }

  _onProjectBuilder(event){
    let selected = event.target.value;
    this.setState({projectBuilder:selected});
  }

  _onProjectBoss(event){
    let selected = event.target.value;
    this.setState({projectBoss:selected});
  }

  _onProjectBossTel(event){
    let selected = event.target.value;
    this.setState({projectBossTel:selected});
  }

  _onContact(event){
    let selected = event.target.value;
    this.setState({contact:selected});
  }

  _onContactTel(event){
    let selected = event.target.value;
    this.setState({contactTel:selected});
  }

  _onStartTime(event){
    let selected = event.target.value;
    this.setState({startTime:selected});
  }

  _onEndTime(event){
    let selected = event.target.value;
    this.setState({endTime:selected});
  }

  _onType(event){
    let selected = event.target.value;
    this.setState({type:selected});
  }

  _onUseCase(event){
    let selected = event.target.value;
    this.setState({useCase:selected});
  }

  _onInvestment(event){
    let selected = event.target.value;
    this.setState({investment:selected});
  }

  _onLicenceId(event){
    let selected = event.target.value;
    this.setState({licenceId:selected});
  }


  _areaChange(event){
    let selected = event.target.value;
    var source = this._makeArea2(selected);
    this.setState({area1:selected,area2Source:source,area2:source[0].value});
    this.setState({area2:source[0].value});
    this._requestWindowId(source[0].value);
  }

  _area2Change(event){
    let selected = event.target.value;
    this.setState({area2:selected});
    this._requestWindowId(selected);
  }

  _area3Change(event){
    let selected = event.target.value;
    this.setState({area3:selected});
  }


  _onCheckForm() {

    console.log(this.state,'state');

    if(this.state.projectName.length <=0){
      this._showAlert('请输入工程名称','确定');
      return;
    }
    if(this.state.projectAddress.length <=0){
      this._showAlert('请输入地址','确定');
      return;
    }
    if(this.state.projectBuilder.length <=0){
      this._showAlert('请输入建设单位','确定');
      return;
    }
    if(this.state.projectBoss.length <=0){
      this._showAlert('请输入法定代表人','确定');
      return;
    }
    if(this.state.projectBossTel.length <=0){
      this._showAlert('请输入法定代表人联系方式','确定');
      return;
    }
    if(this.state.contact.length <=0){
      this._showAlert('请输入联系人','确定');
      return;
    }
    if(this.state.contactTel.length <=0){
      this._showAlert('请输入联系人电话','确定');
      return;
    }
    if(this.state.startTime.length <=0){
      this._showAlert('请输入开工日期','确定');
      return;
    }
    if(this.state.endTime.length <=0){
      this._showAlert('请输入竣工日期','确定');
      return;
    }

    if(this.state.licenceId.length <=0){
      this._showAlert('请输入施工许可证编号','确定');
      return;
    }


    hashHistory.push({pathname:'/assistor',state:this.state})

  }

  render() {

    if(this.state.success)
    {
      return(
          <Page title="在线申办" backRoute='/'>
            <Msg
                type="success"
                title="提交成功"
                description={ '您申办的【' + this.state.item.company+'】已经收到,查询码' + '【'+this.state.backInfo.code+'】' }
            />
          </Page>
      )
    }



    if(this.state.item.id != 1)
    {
      return (
          <Page title='在线办理' backRoute='/'>
            <Msg
                type="warn"
                title="请到【福建消防公众服务网办理】"
                description='微信端暂不支持该功能'
            />
          </Page>
      )
    }

    return (
        <Page title={this.state.item.company} backRoute='/'>
          <Alert
              show={this.state.alert.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
          </Alert>
          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon}
                 iconSize="small">{this.state.showToast.msg}</Toast>

          <CellsTitle>建设单位基本信息</CellsTitle>


          <Form>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>地区:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.area1}  onChange={this._areaChange.bind(this)}>
                  {
                    _.map(this.state.area1Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>

              </CellBody>
              <CellBody>
                <Select defaultValue={this.state.area2}  onChange={this._area2Change.bind(this)}>
                  {
                    _.map(this.state.area2Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>办理窗口:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.area3}  onChange={this._area3Change.bind(this)}>
                  {
                    _.map(this.state.area3Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >工程名称:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.projectName} placeholder="工程名称" onChange={this._onProjectName.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle}>工程地址:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.projectAddress} placeholder="建设工程地址" onChange={this._onProjectAddress.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >建设单位:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.projectBuilder} placeholder="建设单位名称" onChange={this._onProjectBuilder.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >法定代表人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.projectBoss} placeholder="法定代表人/主要负责人" onChange={this._onProjectBoss.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="tel" defaultValue={this.state.projectBossTel} placeholder="法定代表人联系电话" onChange={this._onProjectBossTel.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.contact} placeholder="联系人" onChange={this._onContact.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.contactTel} placeholder="联系人电话号码" onChange={this._onContactTel.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >开工日期:</Label>
              </CellHeader>
              <CellBody>
                <Input type="date" defaultValue={this.state.startTime} onChange={this._onStartTime.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >竣工日期:</Label>
              </CellHeader>
              <CellBody>
                <Input type="date" defaultValue={this.state.endTime} onChange={this._onEndTime.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >类别:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.type}  onChange={this._onType.bind(this)}>
                  {
                    _.map(project_types, (value, key) => {
                      return (
                          <option value={key} key={key}>{value}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >使用性质:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.useCase}  onChange={this._onUseCase.bind(this)}>
                  {
                    _.map(usage_type_for_base_info, (value, key) => {
                      return (
                          <option value={key} key={key}>{value}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyleToLong} >工程投资额(万):</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.investment} onChange={this._onInvestment.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyleToLong}>施工许可证编号:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.licenceId} onChange={this._onLicenceId.bind(this)}/>
              </CellBody>
            </Cell>
          </Form>

          <div>
            <ButtonArea>
              <Button type="primary" onClick={()=>{this._onCheckForm()}}>下一步</Button>
              <Button type="default" onClick={this._goBack.bind(this)}>取消</Button>
            </ButtonArea>
          </div>
        </Page>
    )
  }

}

const styleSheets = {
  orLabel:{
    flexDirection:'row',
    justifyContent:'center',

  },
  textStyle:{
    fontSize:16,
    textAlign:'left',
    marginRight:10,
  },
  textStyleToLong:{
    fontSize:14,
    textAlign:'left',
    marginRight:10,
  },

}

OperationOLUploader.defaultProps = {
  source: Config.api_host + '/client/declarationfiles/submit',
  window:Config.api_host + '/client/serviceCenters?regionId=',
};


OperationOLUploader.contextTypes = {
  router: React.PropTypes.object.isRequired
};
