import React, {Component} from 'react'
import Page from '../components/page'

import {
    ButtonArea,
    Button,
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    MediaBoxInfo,
    MediaBoxInfoMeta,
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    CellsTitle,
    Form,
    FormCell,
    Label,
    Input,
    Select,
    Uploader,
    TextArea,
    Toast,
    Dialog,
    Msg,
} from 'react-weui';
const {Alert} = Dialog;
var Config = require('./Config');
var Ajax = require('../components/ajax');
import WxUploader from '../components/WxImageComponent'
import {AudioUploader} from '../components/AudioUploader'

import {const_parse} from '../components/util';


import {hashHistory} from 'react-router';


var serverUrls = [];

export default class Assistor extends Component {
  constructor(props) {
    super(props);
    this._initialState();
  }


  _initialState() {

    this.state = this.props.location.state;


    console.log('state',this.state);

  }


  componentDidMount() {
    this.context.router.setRouteLeaveHook(
        this.props.route,
        this.routerWillLeave.bind(this)
    )

    // this._requestWindowId(this.state.area2);
  }

  routerWillLeave(nextLocation) {
    // 返回 false 会继续停留当前页面，
    // 否则，返回一个字符串，会显示给用户，让其自己决定
    if (!this.state.isSaved)
      return '改动未提交,确认要离开？';
  }

  _showToast(msg, icon, timer,callback) {
    this.setState({
      showToast: {
        show: true,
        icon: icon,
        msg: msg,
      },
    });
    console.log("timer",timer);
    console.log("callback",callback);

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _showAlert(title, buttonLabel) {
    this.setState({
      alert: {
        showAlert: true,
        title: title,
        buttons: [{label: buttonLabel, onClick: this._hideAlert.bind(this)}]
      }
    });
  }

  _hideAlert() {
    this.setState({alert: {showAlert: false, title: '', buttons: [{label: '', onClick: this._hideAlert.bind(this)}]}});
  }



  _goBack() {
    hashHistory.push({pathname:'/operationOLUploader',state:this.state});
  }





  // _onLicenceId(event){
  //   let selected = event.target.value;
  //   this.setState({licenceId:selected});
  // }

  _onDName(event){
    let selected = event.target.value;
    console.log('name',selected);
    this.setState({designername:selected});
  }

  _onDLevel(event){
    let selected = event.target.value;
    this.setState({designerlevel:selected});
  }

  _onDBoss(event){
    let selected = event.target.value;
    this.setState({designerboss:selected});
  }

  _onDBossTel(event){
    let selected = event.target.value;
    this.setState({designerbossTel:selected});
  }

  _onDContact(event){
    let selected = event.target.value;
    this.setState({designercontact:selected});
  }


  _onOName(event){
    let selected = event.target.value;
    this.setState({operatorname:selected});
  }

  _onOLevel(event){
    let selected = event.target.value;
    this.setState({operatorlevel:selected});
  }

  _onOBoss(event){
    let selected = event.target.value;
    this.setState({operatorboss:selected});
  }

  _onOBossTel(event){
    let selected = event.target.value;
    this.setState({operatorbossTel:selected});
  }

  _onOContact(event){
    let selected = event.target.value;
    this.setState({operatorcontact:selected});
  }

  _onMName(event){
    let selected = event.target.value;
    this.setState({monitorname:selected});
  }

  _onMLevel(event){
    let selected = event.target.value;
    this.setState({monitorlevel:selected});
  }

  _onMBoss(event){
    let selected = event.target.value;
    this.setState({monitorboss:selected});
  }

  _onMBossTel(event){
    let selected = event.target.value;
    this.setState({monitorbossTel:selected});
  }

  _onMContact(event){
    let selected = event.target.value;
    this.setState({monitorcontact:selected});
  }



  _onCheckForm() {

    console.log(this.state.designername);
    if(   this.state.designername.length <=0){
      this._showAlert('请输入设计单位名称','确定');
      return;
    }
    if(this.state.designerlevel.length <=0){
      this._showAlert('请输入设计单位资质','确定');
      return;
    }
    if(this.state.designerboss.length <=0){
      this._showAlert('请输入设计单位法定代表人','确定');
      return;
    }
    if(this.state.designerbossTel.length <=0){
      this._showAlert('请输入设计单位联系电话','确定');
      return;
    }
    if(this.state.designercontact.length <=0){
      this._showAlert('请输入设计单位联系人','确定');
      return;
    }



    if(this.state.operatorname.length <=0){
      this._showAlert('请输入施工单位名称','确定');
      return;
    }
    if(this.state.operatorlevel.length <=0){
      this._showAlert('请输入施工单位资质','确定');
      return;
    }
    if(this.state.operatorboss.length <=0){
      this._showAlert('请输入施工单位法定代表人','确定');
      return;
    }
    if(this.state.operatorbossTel.length <=0){
      this._showAlert('请输入施工单位联系电话','确定');
      return;
    }
    if(this.state.operatorcontact.length <=0){
      this._showAlert('请输入施工单位联系人','确定');
      return;
    }


    if(this.state.monitorname.length <=0){
      this._showAlert('请输入监理单位','确定');
      return;
    }

    if(this.state.monitorlevel.length <=0){
      this._showAlert('请输入监理单位资质','确定');
      return;
    }

    if(this.state.monitorboss.length <=0){
      this._showAlert('请输入监理单位法定代表人','确定');
      return;
    }

    if(this.state.monitorbossTel.length <=0){
      this._showAlert('请输入监理单位联系电话','确定');
      return;
    }
    if(this.state.monitorcontact.length <=0){
      this._showAlert('请输入监理单位联系人','确定');
      return;
    }

    hashHistory.push({pathname:'/fileUploader',state:this.state});

  }

  render() {


    return (
        <Page title='协同单位基本信息' backFunc={this._goBack.bind(this)}>
          <Alert
              show={this.state.alert.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
          </Alert>

          <CellsTitle>设计单位</CellsTitle>
          <Form>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle}>单位名称:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.designername} placeholder="" onChange={this._onDName.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>资质等级:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.designerlevel} placeholder="" onChange={this._onDLevel.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >法定代表人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.designerboss} placeholder="" onChange={this._onDBoss.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle}>联系人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.designercontact} placeholder="" onChange={this._onDContact.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.designerbossTel} placeholder="" onChange={this._onDBossTel.bind(this)}/>

              </CellBody>
            </Cell>
          </Form>
          <CellsTitle>施工单位</CellsTitle>
          <Form>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>单位名称:</Label>
              </CellHeader>
              <CellBody>

                <Input type="text" defaultValue={this.state.operatorname} placeholder="" onChange={this._onOName.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>资质等级:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.operatorlevel} placeholder="" onChange={this._onOLevel.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >法定代表人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.operatorboss} placeholder="" onChange={this._onOBoss.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle}>联系人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.operatorcontact} placeholder="" onChange={this._onOContact.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.operatorbossTel} placeholder="" onChange={this._onOBossTel.bind(this)}/>

              </CellBody>
            </Cell>
          </Form>
          <CellsTitle>监理单位</CellsTitle>
          <Form>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>单位名称:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.monitorname} placeholder="" onChange={this._onMName.bind(this)}/>


              </CellBody>
            </Cell>
            <Cell select selectPos="after">
              <CellHeader>
                <Label style={styleSheets.textStyle}>资质等级:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.monitorlevel} placeholder="" onChange={this._onMLevel.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >法定代表人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.monitorboss} placeholder="" onChange={this._onMBoss.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle}>联系人:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.monitorcontact} placeholder="" onChange={this._onMContact.bind(this)}/>

              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label style={styleSheets.textStyle} >联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.monitorbossTel} placeholder="" onChange={this._onMBossTel.bind(this)}/>

              </CellBody>
            </Cell>
          </Form>

          <div>
            <ButtonArea>
              <Button type="primary" onClick={()=>{this._onCheckForm()}}>下一步</Button>
              <Button type="default" onClick={this._goBack.bind(this)}>取消</Button>
            </ButtonArea>
          </div>
        </Page>
    )
  }

}

const styleSheets = {
  orLabel:{
    flexDirection:'row',
    justifyContent:'center',

  },
  textStyle:{
    fontSize:16,
    textAlign:'left',
    marginRight:10,
  },
  textStyleToLong:{
    fontSize:14,
    textAlign:'left',
    marginRight:10,
  },

}

Assistor.defaultProps = {
  source: Config.api_host + '/client/declarationfiles/submit',
  window:Config.api_host + '/client/serviceCenters?regionId=',
};


Assistor.contextTypes = {
  router: React.PropTypes.object.isRequired
};
