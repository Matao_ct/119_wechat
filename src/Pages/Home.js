import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {
    CellsTitle,
    Cell,
    Cells,
    CellBody,
    Label,
    Msg,
    Toast,
} = WeUI;

import {hashHistory} from 'react-router';

var _ = require('lodash');

var Ajax = require('../components/ajax');
var Config = require('./Config');
import {const_parse} from '../components/util';


class Home extends Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource:{
      },
      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },
    }
  }

  componentDidMount() {
    this._showToast('加载中','loading',2000);
    Ajax.GET(this.props.source ,{})
      .then((res)=>{

        this._makeGroup(res);

        this._showToast('加载中','loading',0);

    }).catch((res)=>{
      this._showToast('加载失败','failed',2000);
    })

  }

  _showToast(msg,icon,timer,callback) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }
  
  _cellClickAction(item){
    console.log('key',item);
    hashHistory.push({pathname:'/guideContent',state:item})  //push({pathname:'/reportStep3',state:this.state})}
  }

  _makeGroup(res){
    var group = {}

    _.map(res,(value,key)=>{
      var findFlag = 0;
      _.map(group,(v,k)=>{
        if(k== value.type){
          findFlag = 1;
          v.push(value);
          return;
        }
      })

      if(findFlag == 0){
        group[value.type] = [value];
      }
    });

    this.setState({dataSource:group});
  }


  render() {
    return (
        <Page title="信息公开">
          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>
          {
            _.map(this.state.dataSource,(value,key)=>{
              return(
                  <div key={key}>
                    <CellsTitle key={key}>{const_parse('homeType', key)}</CellsTitle>
                    <Cells>
                      {
                        _.map(value,(item)=>{
                          return(
                              <Cell key={item.id} onClick={()=>{
                                this._cellClickAction(item)
                              }}><CellBody> <p>{item.company}</p>
                                <Label style={stylesheet.cell_subtitle}>{ item.type == 'DESIGN_CONFIRM' || item.type == 'ACCEPT' || item.type == 'USAGE_PRODUCE' ? item.bookNum:item.code}</Label>
                              </CellBody></Cell>
                          )
                        })
                      }
                    </Cells>
                  </div>
              );
            })
          }
        </Page>
    );
  }
}

const stylesheet = {
  cell_subtitle:{
    fontSize:'14px',
    color:'#95989A',
    width:'100%'
  },
}

export default Home;

Home.defaultProps = {
  source: Config.api_host + '/client/declarations/getSummary',
}
