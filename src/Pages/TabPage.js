/**
 * Created by zhengfw on 16/7/28.
 */
import React from 'react';

import {
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    MediaBoxInfo,
    MediaBoxInfoMeta,
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    Tab,
    TabBody,
    TabBar,
    TabBarItem,
    TabBarIcon,
    TabBarLabel,
    Article,
    Cells,
    CellBody,
    Cell,
    CellsTitle,
    CellFooter,
    Button,
    ButtonArea, Grids, Grid, GridIcon, GridLabel, Icon,
    Label, Toast
} from 'react-weui';

import CustomQuery from './CustomQuery';
import Home from './Home'
import CustomGuide from './CustomGuide';
import Complaints from './Complaints';
import OperationOL from './OperationOL';
import OperationOLList from './OperationOLList';

var Global = require('./global');

var Config = require('./Config');


const barIcon1 = <img src={ Config.static_host + "/wechat_119/static/imageSource/home.png"} alt=""/>
const barIcon2 = <img src={ Config.static_host + "/wechat_119/static/imageSource/daily.png"} alt=""/>
const barIcon3 = <img src={ Config.static_host + "/wechat_119/static/imageSource/affair.png"} alt=""/>
const barIcon4 = <img src={ Config.static_host + "/wechat_119/static/imageSource/mine.png"} alt=""/>



export default class HomePage extends React.Component {
  constructor(props) {
    super(props);

    Config.currentOwner = Config.ownerId;
    // Config.currentOwner = 0; //Todo::测试用,注释掉

    this._initialState();
  }

  _initialState() {
    this.state = {
      tab: Global.home_current_tab,
      records: [],
      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },
      permissions:[],

      currentOwner:Config.currentOwner,

  };
  }


  componentWillMount() {
    ;
  }

  componentWillUnmount() {
    ;
  }

  componentDidMount() {
    ;
  }


  _updateCurrentOwner(ownerId){
    Config.currentOwner = ownerId;

    this.setState({
      // tab:4,
      currentOwner:Config.currentOwner,
    });
  }


  render() {

    // Todo:: 在线申报 如果已经登录 跳过 OperationOL 直接显示List

    console.log('on render',this.state.currentOwner,this.state.tab);

    return (
      <tab type="tabbar">
        <TabBody>
          <div style={{display: this.state.tab == 0 ? null : 'none'}}>
            <Home />
          </div>

          <div style={{display: this.state.tab == 1 ? null : 'none'}}>
            <CustomQuery />
          </div>
          <div style={{display: this.state.tab == 2 ? null : 'none'}}>
            <CustomGuide />
          </div>

          <div style={{display: this.state.tab == 3 ? null : 'none'}}>
            <Complaints />
          </div>

          <div style={{display: this.state.tab == 4 ? null : 'none'}}>
            {
              this.state.currentOwner == 0 ? <OperationOL handleOnClick={(ownerID)=> {
                console.log(ownerID);
                this._updateCurrentOwner(ownerID);
              }}/> : <OperationOLList/>
            }
          </div>

        </TabBody>
        <TabBar style={stylesheet.tabBar}>
          <TabBarItem active={this.state.tab == 0}
                      onClick={()=> {
                        Global.home_current_tab = 0;
                        this.setState({tab: 0})
                      }}

                      icon={barIcon1}
                      label="首页">
          </TabBarItem>
          <TabBarItem active={this.state.tab == 1}
                      onClick={()=> {
                        Global.home_current_tab = 1;
                        this.setState({tab: 1})
                      }}
                      icon={barIcon2}
                      label="办件查询">

          </TabBarItem>
          <TabBarItem active={this.state.tab == 2}
                      onClick={()=> {
                        Global.home_current_tab = 2;
                        this.setState({tab: 2})
                      }}
                      icon={barIcon3}
                      label="常见问题">

          </TabBarItem>
          <TabBarItem active={this.state.tab == 3}
                      onClick={()=> {
                        Global.home_current_tab = 3;
                        this.setState({tab: 3})
                      }}
                      icon={barIcon4}
                      label="投诉建议">

          </TabBarItem>

          <TabBarItem active={this.state.tab == 4}
                      onClick={()=> {
                        Global.home_current_tab = 4;
                        this.setState({tab: 4})
                      }}
                      icon={barIcon4}
                      label="在线申报">

          </TabBarItem>
        </TabBar>
      </tab>
    );
  }
};

HomePage.defaultProps = {

};

const stylesheet = {

  tabBar: {
    position: 'fixed',
  },

  teacherName: {
    font: 14
  },

  headImg: {
    border: '2px solid #fff',
    borderRadius: '50%',
    width: 95,
    height: 95
  },
  divSty: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    margin: '0 auto',
    marginTop: '100px',
    textAlign: 'center',
    marginBottom: -70
  },
};
