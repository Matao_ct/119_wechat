import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const{
    ButtonArea,
    Button,
    CellHeader,
    CellBody,
    Form,
    FormCell,
    Cell,
    Input,
    Label,
    TextArea,
    Msg,
    Toast,
    Dialog,
} = WeUI;


const {Alert} = Dialog;

var Ajax = require('../components/ajax');
var Config = require('./Config');


class Advice extends Component {

  constructor(props){
    super(props);
    this._initialState();
  }

  _initialState(){
    this.state={


      name:'',
      tel:'',
      content:'',

      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },

      alert: {
        showAlert: false,
        title: '标题标题',
        buttons: [
          {
            label: '好的',
            onClick: this._hideAlert.bind(this)
          }
        ]
      },

      success:false,
    }
  }


  _showToast(msg, icon, timer,callback) {
    this.setState({
      showToast: {
        show: true,
        icon: icon,
        msg: msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }


  _showAlert(title, buttonLabel) {
    this.setState({
      alert: {
        showAlert: true,
        title: title,
        buttons: [{label: buttonLabel, onClick: this._hideAlert.bind(this)}]
      }
    });
  }


  _hideAlert() {
    this.setState({alert: {showAlert: false, title: '', buttons: [{label: '', onClick: this._hideAlert.bind(this)}]}});
  }



  _nameChange(event){
    let selected = event.target.value;
    this.setState({name:selected});
  }

  _telChange(event){
    let selected = event.target.value;
    this.setState({tel:selected});
  }

  _contentChange(event){
    let selected = event.target.value;
    this.setState({content:selected});
  }

  _submitAction(){
    if(this.state.name == undefined ||  this.state.name.length <=0){
      console.log('name nil');
      this._showAlert('请输入姓名','取消');
      return;
    }

    if(this.state.tel == undefined ||  this.state.tel.length <=0){
      console.log('name nil');
      this._showAlert('请输入电话号码','取消');
      return;
    }

    if(this.state.content == undefined ||  this.state.content.length <=0){
      this._showAlert('请输入意见建议','取消');
      return;
    }


    var data = {
      name:    this.state.name,
      content: this.state.content,
      mobile:  this.state.tel,

    };


    this._showToast('请求发送中...','loading',-1);

    Ajax.POST(this.props.source,{data:data}).then((res)=>{
      this.setState({success:true})

    }).catch((res)=>{

      this._showToast('操作失败','failed',2000);
    })

  }


  render() {
    if(this.state.success)
    {
      return(
          <Page title="投诉建议" backRoute='/'>
            <Msg
                type="success"
                title="提交成功"
                description="您的反馈已经提交成功,我们会尽快确认并答复"
            />
          </Page>
      )
    }
    return (
        <Page title="意见建议" backRoute='/' >


          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon}
                 iconSize="small">{this.state.showToast.msg}</Toast>
          <Alert
              show={this.state.alert.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
          </Alert>

          <Form>
            <Cell>
              <CellHeader>
                <Label>联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="tel" placeholder="请输入电话号码" onChange={this._telChange.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>姓名:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" placeholder="请输入姓名" onChange={this._nameChange.bind(this)}/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>意见建议:</Label>
              </CellHeader>
              <CellBody>
                <TextArea placeholder="请输入内容" rows="3" maxlength="200" onChange={this._contentChange.bind(this)}/>
              </CellBody>
            </Cell>
          </Form>
          
          <ButtonArea>
            <Button onClick={()=>{this._submitAction()}}>
              提交
            </Button>
          </ButtonArea>
        </Page>
    );
  }
}


Advice.defaultProps = {
  source: Config.api_host + '/client/Advises',
}



export default Advice;
