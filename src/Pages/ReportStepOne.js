import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {ButtonArea,Button,Article} = WeUI;

import {hashHistory} from 'react-router';

class ReportStepOne extends Component {


  constructor(props){
    super(props);
  }


  render() {
    return (
      <Page title="投诉举报" backRoute='/'>

        <Article>
          <section>
            <section>
              <p>1.投诉简要情况栏应如实填写一下内容：投诉的具体单位名称、相关人姓名、事情经过、发生地点和时间等。</p>
            </section>
            <section>
              <p>2.为保护投诉人的合法权利，我们将对投诉人说填写的内容及投诉人个人资料（姓名、性别、身份证号码、联系电话）予以保密。</p>
            </section>
            <section>
              <p>3.投诉人故意捏造事实、诬告陷害相关人员的，依法追究其法律责任。</p>
            </section>
          </section>
        </Article>

        <ButtonArea>
          <Button onClick={()=>{
            {/*this.props.location.state  = {aa:'aa'};*/}
            {/*hashHistory.push('/reportStep2')*/}


            {/*const { history } = this.props;*/}
            hashHistory.push({ pathname: '/reportStep2' });


          }}>下一步</Button>
        </ButtonArea>
      </Page>
    );
  }
}

export default ReportStepOne;
