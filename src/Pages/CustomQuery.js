import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {Button,ButtonArea,Input,CellsTitle,Cells,Cell,Label,Panel,CellBody} = WeUI;


import {hashHistory} from 'react-router';

var Ajax = require('../components/ajax');
var Config = require('./Config');

class CustomQuery extends Component {

  constructor(props){
    super(props);
    this._initialState();
  }

  _initialState(){
    this.state= {sid:'',dataSource:[]};
  }

  _sidChange(event){

    let str = event.target.value;

    console.log(str);
    this._updateSid(str);
  }

  componentWillMount() {

  }

  componentDidMount() {
    Ajax.GET(this.props.source, {})
    .then((res)=> {
      this.setState({dataSource:res.items})
    }).catch((res)=> {

      // this._showToast('加载失败','failed',2000);

    })
  }


  _qrCodeScan(){
    Config.wx.scanQRCode({
      needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
      scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
      success: (res)=> {
        var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果

        console.log(result);

        this._updateSid(result);
        this._goQuery(result);
      }
    })
  }


  _goQuery(sid){

    console.log('state',sid);
    hashHistory.push({pathname:'/queryResult',state:sid})
  }

  _cellClickAction(item){
    console.log('key',item.code);
    this._goQuery(item.code);


  }



  _updateSid(idStr){
    this.setState({sid:idStr});
    // this.forceUpdate();
  }

  render() {



    return (
      <Page title="查询">
        <div style={{flex:2}}>
          <Cells>
            <Cell>
              <Input type="tel" placeholder="输入受理凭证上的序列号" onChange={this._sidChange.bind(this)}/>
            </Cell>
          </Cells>
          <ButtonArea>
            <Button onClick={()=>{this._goQuery(this.state.sid)}}>查询</Button>
          </ButtonArea>
        </div>

        <div style={styleSheets.orLabel}>
          <Label style={styleSheets.textStyle}>或</Label>
        </div>

        <div style={{flex:2}}>
          <ButtonArea>
            <Button onClick={()=>{this._qrCodeScan()}}>扫二维码直接查询</Button>
          </ButtonArea>
        </div>

        <CellsTitle style={styleSheets.list}>{ this.state.dataSource==undefined ||  this.state.dataSource.length <= 0 ? '' : '我的业务'}</CellsTitle>

        <Cells>
          {
            _.map(this.state.dataSource,(value,key)=>{
                return (
                    <Cell key={key}  onClick={()=>{
                      this._cellClickAction(value)
                    }}>
                      <CellBody>
                        <p>{value.company}</p>
                        <Label style={styleSheets.cell_subtitle}> {value.code} </Label>
                      </CellBody>
                    </Cell>
                )
            })
          }

        </Cells>
      </Page>
    );
  }
}

const styleSheets = {
  orLabel:{
    // flexDirection:'row',
    // justifyContent:'center',
    // backgroundColor:'#AA0000',
    marginTop:4,

  },
  textStyle:{
    // flex:1,
    textAlign:'center',
    // backgroundColor:'#AA00FF',
    display: 'block',
    margin: '0 auto'
  },

  list:{
    marginTop:20,
  },
  cell_subtitle:{
    fontSize:'14px',
    color:'#95989A',
    width:'100%'
  },
}

export default CustomQuery;


///client/declarations/getHistory


CustomQuery.defaultProps = {
  source: Config.api_host + '/client/declarations/getHistory',
}
