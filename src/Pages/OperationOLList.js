import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {
    CellsTitle,
    Cell,
    Cells,
    CellBody,
    CellFooter,
    Label,
    Msg,
    Toast,
    Button,
    ButtonArea,
} = WeUI;

import {hashHistory} from 'react-router';

var _ = require('lodash');

var Ajax = require('../components/ajax');
var Config = require('./Config');
import {const_parse} from '../components/util';


class OperationOLList extends Component {



  constructor(props){
    super(props);
    this.state = {
      dataSource:[
        {
          id:1,
          company:'消防设计备案',
        },
        {
          id:2,
          company:'竣工验收消防备案',
        },
        {
          id:3,
          company:'消防设计审核',
        },
        {
          id:4,
          company:'消防验收',
        },
        {
          id:5,
          company:'公共聚集场所使用、营业前消防安全检查',
        },
      ],
      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },
    }
  }

  componentDidMount() {
    // this._showToast('加载中','loading',2000);
    // Ajax.GET(this.props.source ,{})
    // .then((res)=>{

    // this._makeGroup(res);

    //   this._showToast('加载中','loading',0);
    //
    // }).catch((res)=>{
    //   this._showToast('加载失败','failed',2000);
    // })

  }

  _showToast(msg,icon,timer,callback) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _cellClickAction(item){
    console.log('key',item);
    hashHistory.push({pathname:'/operationOLUploader',state:item})  //push({pathname:'/reportStep3',state:this.state})}
  }

  _makeGroup(res){
    var group = {}

    _.map(res,(value,key)=>{
      var findFlag = 0;

      console.log(value);
      _.map(group,(v,k)=>{
        if(k== value.type){
          findFlag = 1;
          v.push(value);
          return;
        }
      })

      if(findFlag == 0){
        group[value.type] = [value];
      }
    });


  }


  render() {
    return (
      <Page title="在线申报">
        <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>
          <CellsTitle >在线申报服务</CellsTitle>
          <Cells>
            {
              _.map(this.state.dataSource,(item)=>{
                return(
                  <Cell access key={item.id} onClick={()=>{this._cellClickAction(item)}} >
                    <CellBody>{item.company}</CellBody>
                    <CellFooter>〉</CellFooter>
                  </Cell>
                )
              })
            }
          </Cells>


        <ButtonArea>
          <Button onClick={()=>{hashHistory.push({pathname:'/sceneGuide'})}}>场景指南</Button>
        </ButtonArea>


      </Page>
    );
  }
}

const stylesheet = {
  cell_subtitle:{
    fontSize:'14px',
    color:'#95989A',
    width:'100%'
  },
}

export default OperationOLList;

OperationOLList.defaultProps = {
  source: Config.api_host + '/client/declarations/getSummary',
}
