import React, {Component} from 'react'
import Page from '../components/page'

import {
    ButtonArea,
    Button,
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    MediaBoxInfo,
    MediaBoxInfoMeta,
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    CellsTitle,
    Form,
    FormCell,
    Label,
    Input,
    Select,
    Uploader,
    TextArea,
    Toast,
    Dialog,
    Msg,
} from 'react-weui';
const {Alert} = Dialog;
var Config = require('./Config');
var Ajax = require('../components/ajax');
import WxUploader from '../components/WxImageComponent'
import {AudioUploader} from '../components/AudioUploader'

import {const_parse} from '../components/util';

import {hashHistory} from 'react-router';

export default class FilesUploader extends Component {
  constructor(props) {
    super(props);
    this._initialState();
  }


  _initialState() {

    this.state = this.props.location.state;

    // this.state = {
    //
    //   area1:area1[0].value,
    //   area2:area2[0].value,
    //   area3:'',
    //   area1Source:area1,
    //   area2Source:area2,
    //   area3Source:[],
    //   dataSource: {
    //     sheets:[],
    //     businessLicense:[],
    //   },
    //   item:this.props.location.state,
    //
    //   showToast: {
    //     show: false,
    //     icon: null,
    //     msg: null,
    //     timer: null,
    //   },
    //
    //   isSaved: true,
    //
    //   alert: {
    //     showAlert: false,
    //     title: '标题标题',
    //     buttons: [
    //       {
    //         label: '好的',
    //         onClick: this._hideAlert.bind(this)
    //       }
    //     ]
    //   },
    //
    //   backInfo:{
    //     deadLine:'',
    //     code:''
    //   },
    //   success:false,
    // }
  }


  componentDidMount() {
    this.context.router.setRouteLeaveHook(
        this.props.route,
        this.routerWillLeave.bind(this)
    )
  }


  routerWillLeave(nextLocation) {
    // 返回 false 会继续停留当前页面，
    // 否则，返回一个字符串，会显示给用户，让其自己决定
    if (!this.state.isSaved)
      return '改动未提交,确认要离开？';
  }


  _showToast(msg, icon, timer,callback) {
    this.setState({
      showToast: {
        show: true,
        icon: icon,
        msg: msg,
      },
    });
    console.log("timer",timer);
    console.log("callback",callback);

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _showAlert(title, buttonLabel) {
    this.setState({
      alert: {
        showAlert: true,
        title: title,
        buttons: [{label: buttonLabel, onClick: this._hideAlert.bind(this)}]
      }
    });
  }

  _hideAlert() {
    this.setState({alert: {showAlert: false, title: '', buttons: [{label: '', onClick: this._hideAlert.bind(this)}]}});
  }

  _changeImageFile(newFiles) {
    this.state.isSaved = false;
    console.log("sheetFiles " , newFiles);
    this.state.dataSource.sheets = newFiles;
  }

  _changeBusinessFile(newFiles) {
    this.state.isSaved = false;
    console.log("businessLicense " , newFiles);
    this.state.dataSource.businessLicense = newFiles;
  }


  _goBack() {
    hashHistory.push({pathname:'/assistor',state:this.state});
  }


  _makeFileName(ds){

    var nameDs = [];
    _.map(ds,(value,key)=>{
      var obj = {};
      obj['name'] = 'fileName.jpg';
      obj['url'] = value;

      nameDs.push(obj);
    })

    return nameDs;
  }

  _onSave() {

    console.log('save onclick',this.state);

    if(this.state.dataSource.sheets.length <= 0){
      console.log('show alert a');
      this._showAlert('请上传'+this.state.item.company,'取消');
      return;
    }
    if(this.state.dataSource.businessLicense.length <= 0){
      console.log('show alert b');
      this._showAlert('请上传合法身份证明文件','取消');
      return;
    }
    console.log('save onclick gogogg');


    this._showToast('提交中...', 'loading', -1);

    AudioUploader(this.state.dataSource.sheets)
    .then((sheets) => {
      console.log("上传 sheets " , sheets);
      AudioUploader(this.state.dataSource.businessLicense)
      .then((businessLicense) => {

        console.log("上传 businessLicense " , businessLicense);


        console.log('state',this.state);

        var data = {

          type:const_parse('uploadType',this.state.item.id),
          windowId:this.state.area3,
          projectType:this.state.type,
          userCase:[this.state.userCase],
          areaId:this.state.area3,

          company:this.state.projectBuilder,
          chargeMan:this.state.projectBoss,
          chargeManTel:this.state.projectBossTel,
          project:this.state.projectName,
          linkMan:this.state.contact,
          linkManTel:this.state.contactTel,
          planBeginBuildDate:this.state.startTime,
          planEndBuildDate:this.state.endTime,
          address:this.state.projectAddress,
          investment:this.state.investment,
          buildAllowCode:this.state.licenceId,
          note:'',


          ownerId:Config.ownerId,
          files:{items:[
            {
              id:'A4',
              name:'建设工程施工许可文件',
              files:this._makeFileName(sheets),
              
            },
            {
              id:'A1',
              files:this._makeFileName(businessLicense),
              name:'建设单位的工商营业执照等合法身份证明文件',
            }
          ]},

          cooperativeUnits:[
            {
              type:'DESIGN',
              level:this.state.designerlevel,
              title:this.state.designername,
              chargeMan:this.state.designerboss,
              linkMan:this.state.designercontact,
              linkManTel:this.state.designerbossTel,
            },
            {
              type:'BUILD',
              level:this.state.operatorlevel,
              title:this.state.operatorname,
              chargeMan:this.state.operatorboss,
              linkMan:this.state.operatorcontact,
              linkManTel:this.state.operatorbossTel,
            },
            {
              type:'MONITOR',
              level:this.state.monitorlevel,
              title:this.state.monitorname,
              chargeMan:this.state.monitorboss,
              linkMan:this.state.monitorcontact,
              linkManTel:this.state.monitorbossTel,
            },
          ]
        };



        Ajax.POST(this.props.source, {data: data})
        .then((res) => {
          this.state.isSaved = true;

          console.log('res',res);

          this._showToast('提交成功', 'loading', 0);

          this.setState({
            success:true,
            backInfo:{
              code:res.code,
              deadLine:res.limitDays,
            }
          })


        })
        .catch((res) => {
          console.log('failed',res);
          this._showToast('提交失败', 'loading', 2000);
        })



      })
      .catch((error) => {

        console.log('failed ' + error);
        this._showToast('提交失败', 'failed', 2000);
      })
    })
    .catch((error) => {
      console.log('failed ' + error);

      this._showToast('提交失败:' + error, 'loading', 2000);
    });
  }

  render() {

    if(this.state.success)
    {
      return(
          <Page title="在线申办" backRoute='/'>
            <Msg
                type="success"
                title="提交成功"
                description={ '您申办的【' + this.state.item.company+'】已经收到,查询码' + '【'+this.state.backInfo.code+'】' }
            />
          </Page>
      )
    }



    if(this.state.item.id != 1)
    {
      return (
          <Page title='在线办理' backRoute='/'>
            <Msg
                type="warn"
                title="请到【福建消防公众服务网办理】"
                description='微信端暂不支持该功能'
            />
          </Page>
      )
    }

    return (
        <Page title='文件上传' backFunc={this._goBack.bind(this)}>
          <Alert
              show={this.state.alert.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
          </Alert>
          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon}
                 iconSize="small">{this.state.showToast.msg}</Toast>

          <CellsTitle>{this.state.item.company}</CellsTitle>


          <Form>
            <Cell>
              <CellBody>
                <WxUploader
                    title='上传'
                    maxCount={4}
                    afeterChange={(files) => {
                      this._changeImageFile(files)
                    }}
                />
              </CellBody>
            </Cell>
          </Form>
          <CellsTitle>建设单位的工商营业执照等合法身份证明文件</CellsTitle>
          <Form>
            <Cell>
              <CellBody>
                <WxUploader
                    title='上传'
                    maxCount={6}
                    afeterChange={(files) => {
                      this._changeBusinessFile(files)
                    }}
                />
              </CellBody>
            </Cell>
          </Form>
          <div>
            <ButtonArea>
              <Button type="primary" onClick={()=>{this._onSave()}}>提交</Button>
              <Button type="default" onClick={this._goBack.bind(this)}>取消</Button>
            </ButtonArea>
          </div>
        </Page>
    )
  }

}

FilesUploader.defaultProps = {
  source: Config.api_host + '/client/declarations',
  window:Config.api_host + '/client/serviceCenters?regionId=',
};


FilesUploader.contextTypes = {
  router: React.PropTypes.object.isRequired
};
