import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {
    CellsTitle,
    Cell,
    Cells,
    CellBody,
    CellFooter,
    Label,
    Msg,
    Toast,
    Button,
    ButtonArea,
} = WeUI;

import {hashHistory} from 'react-router';

var _ = require('lodash');

var Ajax = require('../components/ajax');
var Config = require('./Config');
import {const_parse} from '../components/util';


const allData = {
      name:'',
      title:'选择建筑性质',
      items:[
        {
          id:1,
          title:'选择办理情况',
          name:'经营场所或大型人员密集场所',
            items:[{
              id:1,
              title:'用途类型',
              name:'初次申办',
                items:[
                  {
                    id:1,
                    title:'面积',
                    name:'歌舞厅、录像厅、放映厅、卡拉ＯＫ厅、夜总会、游艺厅、桑拿浴室、网吧、酒吧，具有娱乐功能的餐馆、茶馆、咖啡厅',
                    items:[
                      {
                        id:1,
                        name:'小于500平方米',
                        plane:{
                          id:1,
                          company:'消防设计备案',
                        }
                      },
                      {
                        id:2,
                        name:'大于500平方米',
                        plane:{
                          id:3,
                          company:'消防设计审核',
                        },
                      }
                    ]
                  },
                  {
                    id:2,
                    name:'托儿所、幼儿园的儿童用房，儿童游乐厅等室内儿童活动场所，养老院、福利院，医院、疗养院的病房楼，中小学校的教学楼、图书馆、食堂，学校的集体宿舍，劳动密集型企业的员工集体宿舍；',
                    items:[
                      {
                        id:1,
                        name:'小于500平方米',
                          plane:{
                            id:1,
                            company:'消防设计备案',
                          }
                      },
                      {
                        id:2,
                        name:'大于500平方米',

                          plane:{
                            id:3,
                            company:'消防设计审核',
                          }
                      }
                    ]
                  },
                  {
                    id:3,
                    name:'影剧院，公共图书馆的阅览室，营业性室内健身、休闲场馆，医院的门诊楼，大学的教学楼、图书馆、食堂，劳动密集型企业的生产加工车间，寺庙、教堂；',
                    plane:{
                      id:3,
                      company:'消防设计审核',
                    },
                  },
                  {
                    id:4,
                    name:'宾馆、饭店、商场、市场；',
                    plane:{
                      id:3,
                      company:'消防设计审核',
                    },
                  },
              ]},
              {
                id:2,
                title:'用途类型',
                name:'开业前',
                items:[
                  {
                    id:1,
                    title:'面积',
                    name:'歌舞厅、录像厅、放映厅、卡拉ＯＫ厅、夜总会、游艺厅、桑拿浴室、网吧、酒吧，具有娱乐功能的餐馆、茶馆、咖啡厅',
                    items:[
                      {
                        id:1,
                        name:'小于500平方米',
                        plane:{
                          id:1,
                          company:'消防设计备案',
                        }
                      },
                      {
                        id:2,
                        name:'大于500平方米',
                        plane:{
                          id:3,
                          company:'消防设计审核',
                        },
                      }
                    ]
                  },
                  {
                    id:2,
                    name:'托儿所、幼儿园的儿童用房，儿童游乐厅等室内儿童活动场所，养老院、福利院，医院、疗养院的病房楼，中小学校的教学楼、图书馆、食堂，学校的集体宿舍，劳动密集型企业的员工集体宿舍；',
                    items:[
                      {
                        id:1,
                        name:'小于500平方米',
                        plane:{
                          id:1,
                          company:'消防设计备案',
                        }
                      },
                      {
                        id:2,
                        name:'大于500平方米',

                        plane:{
                          id:3,
                          company:'消防设计审核',
                        }
                      }
                    ]
                  },
                  {
                    id:3,
                    name:'影剧院，公共图书馆的阅览室，营业性室内健身、休闲场馆，医院的门诊楼，大学的教学楼、图书馆、食堂，劳动密集型企业的生产加工车间，寺庙、教堂；',
                    plane:{
                      id:3,
                      company:'消防设计审核',
                    },
                  },
                  {
                    id:4,
                    name:'宾馆、饭店、商场、市场；',
                    plane:{
                      id:3,
                      company:'消防设计审核',
                    },
                  },
                ]},
            ]
        },
        {
          id:2,
          title:'工程类型',
          name:'特殊工程',
          items:[
            {
              id:1,
              title:'面积',
              name:'国家机关办公楼,电力调度楼,电信楼,邮政楼,防灾指挥调度楼,广播电视楼,档案楼',
              plane:{
                id:3,
                company:'消防设计审核',
              },
            },
            {
              id:2,
              name:'国家标准规定的一类高层住宅建筑',
              plane:{
                id:3,
                company:'消防设计审核',
              },
            },
            {
              id:3,
              name:'城市轨道交通工程,城市隧道工程,大型发电工程,大型配变电工程',
              plane:{
                id:3,
                company:'消防设计审核',
              },
            },
            {
              id:4,
              name:'生产、储存易燃易爆危险物品的工厂、专用码头,装卸易燃易爆危险物品的专用车站、码头,易燃易爆危险物品的充装站、供应站、调压站',
              plane:{
                id:3,
                company:'消防设计审核',
              },
            },
          ]
        }
      ]
    }


class SceneGuide extends Component {



  constructor(props){
    super(props);
      this.state = {
        dataSource:allData,
      }
    console.log('props',this.state);
  }

  componentDidMount() {

  }

  _showToast(msg,icon,timer,callback) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _cellClickAction(item){
    console.log('key',item);
    this.setState({
      dataSource:item,
    })
  }


  render() {

    if(this.state.dataSource.plane){
      return(
          <Page title="场景化路径指南" backRoute="/">

            <Cells>
              <Cell>适用填写《{this.state.dataSource.plane.company}》</Cell>
            </Cells>

            <ButtonArea>
              <Button onClick={()=>{hashHistory.push({pathname:'/operationOLUploader',state:this.state.dataSource.plane})}}>去填写</Button>
            </ButtonArea>

          </Page>
      )
    }




    return (
        <Page title="场景化路径指南" backRoute="/">
          <CellsTitle >{this.state.dataSource.title}</CellsTitle>
          <Cells>
            {
              _.map(this.state.dataSource.items,(item)=>{
                return(
                    <Cell access key={item.id} onClick={()=>{this._cellClickAction(item)}} >
                      <CellBody>{item.name}</CellBody>
                      <CellFooter>〉</CellFooter>
                    </Cell>
                )
              })
            }
          </Cells>
        </Page>
    );
  }
}

const stylesheet = {
  cell_subtitle:{
    fontSize:'14px',
    color:'#95989A',
    width:'100%'
  },
}

export default SceneGuide;

SceneGuide.defaultProps = {
  source: Config.api_host + '/client/declarations/getSummary',
}
