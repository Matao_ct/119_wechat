import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {Grids,Grid,GridLabel,GridIcon,} = WeUI;

import {hashHistory} from 'react-router';

var Config = require('./Config')

const icon_xunke =      <img src={ Config.static_host + "/wechat_119/static/imageSource/home/patrol.svg"} alt=""/>
const icon_xunkejilu =  <img src={ Config.static_host + "/wechat_119/static/imageSource/home/patrolrecord.svg"} alt=""/>

class Complaints extends Component {
  render() {
    return (
      <Page title="投诉建议">
        <Grids>
          <Grid onClick={()=> {hashHistory.push('advice')}}>
            <GridIcon>{icon_xunke}</GridIcon>
            <GridLabel>意见建议</GridLabel>
          </Grid>
          <Grid onClick={()=> {hashHistory.push('reportStep1')}}>
            <GridIcon>{icon_xunkejilu}</GridIcon>
            <GridLabel>投诉举报</GridLabel>
          </Grid>
        </Grids>
      </Page>
    );
  }
}



export default Complaints;
