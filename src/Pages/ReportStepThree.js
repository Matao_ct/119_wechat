import React, { Component } from 'react';


import Page from '../components/page';


import {hashHistory} from 'react-router';

import WeUI from 'react-weui';
import 'weui';
const{
    ButtonArea,
    Button,
    CellHeader,
    CellBody,
    Form,
    FormCell,
    Cell,
    Input,
    Label,
    TextArea,
    CellsTitle,
    Msg,
    Dialog,
    Toast
} = WeUI;

const {Alert} = Dialog;

var Ajax = require('../components/ajax');
var Config = require('./Config');

import {const_parse} from '../components/util';

class ReportStepThree extends Component {


  constructor(props){
    super(props);
    this._initialState();
  }

  _initialState(){
    this.state = {
      area:this.props.location.state.area,
      type:this.props.location.state.type,
      content:this.props.location.state.content,

      name:this.props.location.state.name,
      sID:this.props.location.state.sID,
      tel:this.props.location.state.tel,
      addr:this.props.location.state.addr,

      area1:this.props.location.state.area1,
      area2:this.props.location.state.area2,

      area1Source:this.props.location.state.area1Source,
      area2Source:this.props.location.state.area2Source,


      success:false,

      alert: {
        showAlert: false,
        title: '标题标题',
        buttons: [
          {
            label: '好的',
            onClick: this._hideAlert.bind(this)
          }
        ]
      },

      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },


    }
  }

  _showToast(msg, icon, timer,callback) {
    this.setState({
      showToast: {
        show: true,
        icon: icon,
        msg: msg,
      },
    });
    console.log("timer",timer);
    console.log("callback",callback);

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }


  _nameChange(event){
    let selected = event.target.value;
    this.setState({name:selected});
  }

  _sIDChange(event){
    let selected = event.target.value;
    this.setState({sID:selected});
  }

  _addrChange(event){
    let selected = event.target.value;
    this.setState({addr:selected});
  }

  _telChange(event){
    let selected = event.target.value;
    this.setState({tel:selected});
  }

  _backRoute(){
    hashHistory.push({pathname: '/reportStep2',state:this.state});
  }


  _showAlert(title, buttonLabel) {
    this.setState({
      alert: {
        showAlert: true,
        title: title,
        buttons: [{label: buttonLabel, onClick: this._hideAlert.bind(this)}]
      }
    });
  }


  _hideAlert() {
    this.setState({alert: {showAlert: false, title: '', buttons: [{label: '', onClick: this._hideAlert.bind(this)}]}});
  }


  _submitAction(){

    if(this.state.name == undefined ||  this.state.name.length <=0){
      console.log('name nil');
      this._showAlert('请输入姓名','取消');
      return;
    }

    if(this.state.sID == undefined ||  this.state.sID.length <=0){
      console.log('name nil');
      this._showAlert('请输入身份证','取消');
      return;
    }

    if(this.state.addr == undefined ||  this.state.addr.length <=0){
      console.log('name nil');
      this._showAlert('请输入地址','取消');
      return;
    }

    if(this.state.tel == undefined ||  this.state.tel.length <=0){
      console.log('name nil');
      this._showAlert('请输入电话号码','取消');
      return;
    }

    if(this.state.content == undefined ||  this.state.content.length <=0){
      this._showAlert('请输入投诉内容','取消');
      return;
    }


    var data = {
      area:             this.state.area2,
      complaintType:    const_parse('complaintsType', this.state.type),
      complaintContent: this.state.content,
      name:             this.state.name,
      mobile:           this.state.tel,
      identityId:       this.state.sID,
      address:          this.state.addr,

    };


    this._showToast('请求发送中...','loading',-1);

    Ajax.POST(this.props.source,{data:data}).then((res)=>{
      this.setState({success:true})

    }).catch((res)=>{

      this._showToast('操作失败','failed',2000);
    })



  }

  render() {

    if(this.state.success)
    {
      return(
          <Page title="投诉建议" backRoute='/'>
            <Msg
                type="success"
                title="提交成功"
                description="您的反馈已经提交成功,我们会尽快核实并处理"
            />
          </Page>
      )
    }

    return (
        <Page title="投诉建议" backFunc={this._backRoute.bind(this)}>
          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon}
                 iconSize="small">{this.state.showToast.msg}</Toast>
          <Alert
              show={this.state.alert.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
          </Alert>
          <CellsTitle>填写个人信息</CellsTitle>

          <Form>
            <Cell>
              <CellHeader>
                <Label>姓名:</Label>
              </CellHeader>
              <CellBody>
                <Input type="text" defaultValue={this.state.name} onChange={this._nameChange.bind(this)} placeholder="请输入姓名"/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>身份证号:</Label>
              </CellHeader>
              <CellBody>
                <Input type="tel" defaultValue={this.state.sID} onChange={this._sIDChange.bind(this)} placeholder="请输入身份证号码"/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>联系电话:</Label>
              </CellHeader>
              <CellBody>
                <Input type="tel" defaultValue={this.state.tel} onChange={this._telChange.bind(this)} placeholder="请输入电话号码"/>
              </CellBody>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>联系地址:</Label>
              </CellHeader>
              <CellBody>
                <TextArea placeholder="请输入地址" defaultValue={this.state.addr} onChange={this._addrChange.bind(this)} rows="2" maxlength="200" />
              </CellBody>
            </Cell>
          </Form>

          <ButtonArea>
            <Button onClick={()=>{this._submitAction()}}>
              提交
            </Button>
          </ButtonArea>


        </Page>
    );
  }
}


ReportStepThree.defaultProps = {
  source: Config.api_host + '/client/Complaints',
}

export default ReportStepThree;
