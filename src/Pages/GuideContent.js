import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {Article} = WeUI;

import {const_parse} from '../components/util';


class GuideContent extends Component {


  constructor(props){
    super(props);
    this._initialState();
  }

  _initialState(){
    this.state = this.props.location.state;

    console.log(this.state);
  }


  render() {
    return (
        <Page title="详情" backRoute='/'>
          <Article>

            <h1>{this.state.title!=undefined?this.state.title:  this.state.company}</h1>
            <h3>{ this.state.type == 'DESIGN_CONFIRM' || this.state.type == 'ACCEPT' || this.state.type == 'USAGE_PRODUCE' ? this.state.bookNum:this.state.code}</h3>
            <h3>{this.state.updateTime}</h3>

            <section>
              <section>&nbsp;</section>
              <section>{this.state.content!=undefined?this.state.content:this.state.project}</section>
              <section>{this.state.address}</section>
              <section>{this.state.result!=undefined?this.state.result: const_parse('queryStatus', this.state.status)}</section>
            </section>
          </Article>
        </Page>
    );
  }
}

export default GuideContent;
