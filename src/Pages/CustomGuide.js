import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {Cells,Cell,CellsTitle,Toast} = WeUI;
var _ = require('lodash');

import {hashHistory} from 'react-router';

var Ajax = require('../components/ajax');
var Config = require('./Config');

class CustomGuide extends Component {


  constructor(props){
    super(props);
    this.state = {
      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },
    dataSource:{
    }
  }
  }

  _showToast(msg,icon,timer,callback) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  _makeGroup(res){


    var group = {

    }

    _.map(res.items,(value,key)=>{

      var findFlag = 0;
     _.map(group,(v,k)=>{
      if(k== value.questionType){
        findFlag = 1;
        v.push(value);
        return;
      }
      else {

      }
     })

      if(findFlag == 0){
        group[value.questionType] = [value];
      }

    });

    this.setState({dataSource:group});

  }

  componentDidMount() {
    this._showToast('加载中','loading',-1)
    Ajax.GET(this.props.source, {})
    .then((res)=> {

      this._showToast('加载中','loading',0);
      this._makeGroup(res);
    }).catch((res)=> {

      this._showToast('加载失败','failed',2000);

    })
  }

  _cellClickAction(item){
    console.log('key',item);
    hashHistory.push({pathname:'/guideContent',state:item})  //push({pathname:'/reportStep3',state:this.state})}
  }

  render() {


    return (
      <Page title="常见问题">
        <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>

        {
          _.map(this.state.dataSource,(value,key)=>{
            return(
            <div key={key}>
              <CellsTitle key={key}>{key}</CellsTitle>
              <Cells>
                {
                  _.map(value,(item)=>{
                    return(
                        <Cell key={item.id} onClick={()=>{
                          this._cellClickAction(item)
                        }}>{item.title}</Cell>
                    )
                  })
                }
              </Cells>
            </div>
            );
          })
        }
      </Page>
    );
  }
}

const styleSheets = {
  orLabel:{
    flexDirection:'row',
    justifyContent:'center',

  },
  textStyle:{
    flex:1,
    textAlign:'center',
  }
}

export default CustomGuide;

CustomGuide.defaultProps = {
  source: Config.api_host + '/client/faqs',
}