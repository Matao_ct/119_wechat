import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';
const {Panel,PanelBody,MediaBox,MediaBoxDescription,Cells,Cell,Label,CellBody,Toast,Msg} = WeUI;

var Ajax = require('../components/ajax');
var Config = require('./Config');

import {const_parse} from '../components/util';

class CustomQueryResult extends Component {


  constructor(props) {
    super(props);
    this.state = {
      showToast: {
        show: false,
        icon: null,
        msg: null,
        timer: null,
      },
      dataSource: {
        code: this.props.location.state,
        status: '',
        createTime: '',
        widowId: '',
        leftDays:'',
        aduits: [],
      },
      success:true,
    }
  }

  _showToast(msg,icon,timer,callback) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if (timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show: false}});
        if(callback){
          callback();
        }
      }, timer);
  }

  componentDidMount() {

    console.log('queryResult',this.props.location.state);

    this._showToast('加载中', 'loading', -1);

    Ajax.GET(this.props.source + this.props.location.state, {})
      .then((res)=> {
          console.log(res.audits);

        this.setState({dataSource:res});
        this._showToast('加载中', 'loading', 0);

      })
      .catch((res)=>{
          console.log(res);
        this.setState({success:false});
      }
    )
  }




  render() {

    if(!this.state.success)
    {
      return(
          <Page title="查询结果" backRoute='/'>
            <Msg
                type="warn"
                title="无查询结果"
                description="请返回重新输入"
            />
          </Page>
      )
    }


    return (
        <Page title="查询结果" backRoute='/'>
          <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>
          <Panel>
            <PanelBody>
              <MediaBox type="text">
                <MediaBoxDescription>
                  编号:{this.state.dataSource == null ? '' : this.state.dataSource.code}
                </MediaBoxDescription>
                <MediaBoxDescription>
                  状态:{const_parse('queryStatus', this.state.dataSource.status)}
                </MediaBoxDescription>
                <MediaBoxDescription>
                  期限:剩余{this.state.dataSource.leftDays}个工作日
                </MediaBoxDescription>
                <MediaBoxDescription>
                  咨询电话:0591-87089119
                </MediaBoxDescription>
              </MediaBox>
            </PanelBody>
          </Panel>

          <Cells>
            {
              _.map(this.state.dataSource.audits,(item,key)=>{
                return(
                    <Cell key={item.id}>
                      <CellBody>
                        <Label style={styleSheet.cell_title}>{item.content}</Label>
                        <Label style={styleSheet.cell_subtitle}>{item.updateTime}</Label>
                      </CellBody>
                    </Cell>)})
              }
          </Cells>
        </Page>
    );
  }
}

const styleSheet ={

  cell_title:{
    fontSize:'16px',
    color:'F0F0F0',
    width:'100%',
  },

  cell_subtitle:{
    fontSize:'14px',
    color:'#95989A',
    width:'100%'
  },
}



export default CustomQueryResult;


CustomQueryResult.defaultProps = {
  source: Config.api_host + '/client/declarations?code=',
}