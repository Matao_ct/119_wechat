import React, {Component} from 'react'
import Page from '../components/page'

import WeUI from 'react-weui';
import 'weui';
const {
    ButtonArea,
    Button,
    CellHeader,
    CellBody,
    CellFooter,
    Form,
    Cell,
    Input,
    Label,
    Select,
    Toast,
    Dialog
} = WeUI;


const {Confirm} = Dialog;

var Config = require('./Config');
var Ajax = require('../components/ajax');
var moment = require('moment');


class CountDownBtn extends Component{
  static propTypes = {
    handleOnClick: React.PropTypes.func
  }

  constructor(props){
    super(props);

    this.state={
      countdown: 60,
      enable: false,
      canSend: true,
      countText: "验证码"
    }
  }

  handleClick() {
    //根据外面props.enableCountDown 决定点击后是否开始倒计时
    if (this.props.handleOnClick)
      this.props.handleOnClick(this);

  }

  countDown() {
    this.setState({
      countdown: 60,
      enable: true,
      canSend: false,
      countText: "60秒"
    });
    //此处切记，在setInterval里，this指的是计时器的作用域，因此用that表示click的作用域
    if (this.state.enable) {
      var that = this;
      var timeCounter = setInterval(function () {
        that.state.countdown--;
        that.state.countText = that.state.countdown + "秒";
        if (0 === that.state.countdown) {
          that.setState({
            countdown: 60,
            enable: true,
            canSend: true,
            countText: "验证码"
          });
          clearInterval(timeCounter);
        } else {
          //一开始用的是that.setState();
          that.forceUpdate();
        }
      }, 1000);
    }
  }

  render () {
    if (this.state.enable && this.state.canSend) {
      return (<Button type="primary" size="small" plain
                     onClick={ ()=>{ this.handleClick()}}>
        验证码
      </Button>)
    } else {
      return (<Button type="default" size="small" plain disabled>
        {this.state.countText}
      </Button>)
    }
  }
}

class OperationOL extends Component {

  static propTypes = {
    handleOnClick: React.PropTypes.func
  }


  constructor(props) {
    super(props);
    this._initialState();

    console.log(this.refs);
  }

  _makeArea1(){
    var area1 = [];
    _.map(Config.area,(value,key) =>{
      var item = {};
      item['value'] = value.id;
      item['label'] = value.name;
      area1.push(item);
    });
    return area1;
  }


  handleClick(ownerId) {

    Config.currentWindow = this.state.area2;

    if (this.props.handleOnClick)
      this.props.handleOnClick(ownerId);

  }

  _makeArea2(area1){

    console.log('area1 select',area1);

    var source = _.filter(Config.area,(value,key)=>{
      if( value.id == area1)
        return value;
    })

    var area2 = [];

    _.map(source[0].children,(value,key)=>{
      var item ={};
      item['value'] = value.id;
      item['label'] = value.name;

      area2.push(item);
    })
    return area2;
  }

  _initialState() {

    var area1 = this._makeArea1();
    var area2 = this._makeArea2(area1[0].value);

    this.state = {
      area1:area1[0].value,
      area2:area2[0].value,
      area1Source:area1,
      area2Source:area2,
      enableNext: false,
      imageCode: null,
      inputPhone: null,
      inputCode: null,
      showToast: false,
      toastTimer: false,
      toastInfo: null,
      toastIcon: null,
      imageCodeUrl:this.props.imageCodeUrl+moment().format('X'),
      enableCountDown:false,
      showConfirm: false,
      confirm: {
        title: '一直没有收到短信验证码?',
        buttons: [
          {
            type: 'default',
            label: '不是的',
            onClick: this.nagativeConfirm.bind(this)
          },
          {
            type: 'primary',
            label: '是啊',
            onClick: this.positiveConfirm.bind(this)
          }
        ]
      },
      staticVar: {
        sendCodeTimes: 0,
        failReportTimes: 0,
      }
    };
  }

  componentWillUnmount() {
    this.state.toastTimer && clearTimeout(this.state.toastTimer);
  }


  _bindPhone() {
    if (this.state.enableNext) {
      this.showToastNow({
        icon: 'loading',
        text: '正在发送请求...',
        duration: 0
      });
      var data = {
        mobile: this.state.inputPhone,
        code: this.state.inputCode,
        area: this.state.area2,
      }
      if (this.props.bindSource !== '') {
        Ajax.GET(this.props.bindSource, {data: data})
        .then((res) => {
          this.showToastNow({
            icon: 'toast',
            text: '登录成功',
            duration: 3000
          });

          this.handleClick(15);


        })
        .catch((res) => {
          this.showToastNow({
            icon: 'loading',
            text: res.msg,
            duration: 2000
          });
        })
        .then(() => { //finally
          this.setState({
            showLoading: false
          })
        });
      }
    }
  }

  _sendCode() {
    //请求验证码之前,提示用户输入图形验证码
    if (this.state.imageCode ==null || this.state.imageCode.length!=4) {
      this.showToastNow({
        icon: 'warn',
        text: '请输入4位图形验证码',
        duration: 3000
      });
      return;
    } else {
      ;
    }


    let data = {
      mobile: this.state.inputPhone,
      code: this.state.imageCode,
    }
    this.showToastNow({
      icon: 'loading',
      text: '正在请求图形验证码',
      duration: -1
    });
    if (this.props.sendSource !== '') {
      this.sendRequest = Ajax.POST(this.props.sendSource, {data: data})
      .then((res) => {
        //请求成功后,倒计时开始
        this.refs.countDownBtn.countDown();

        if (this.state.staticVar.sendCodeTimes >= 3) {//如果请求超过三次验证码,提醒是否未收到短信登记
          this.setState({
            showConfirm: true,
          })
        }
        this.state.staticVar.sendCodeTimes++;

        this.showToastNow({
          icon: 'toast',
          text: '验证码发送成功',
          duration: 2000
        });
      })
      .catch((res) => {
        this.refresh();//刷新图形验证码
        this.showToastNow({
          icon: 'loading',
          text: res.msg,
          duration: 2000
        });
      })
      .then(() => { //finally
        ;
      });
    }
  }
  checkImageCodeInput(event){
    let inputValue = event.target.value;
    if (inputValue.length == 4) {
      this.setState({enableCountDown:true});
    }else{
      this.setState({enableCountDown:false});
    }
    this.setState({imageCode: inputValue});
  }
  refresh(){
    let url=this.props.imageCodeUrl+moment().format('X');
    this.setState({
      imageCodeUrl:url,
    });
  }
  checkNumberInput(event) {
    let inputValue = event.target.value;
    this.setState({inputPhone: inputValue});
    if (inputValue.length == 11) {
      this.refs.countDownBtn.setState({enable: true});
    } else {
      this.refs.countDownBtn.setState({enable: false});
    }
    this.checkEnableNext();
  }

  checkCodeInput(event) {
    let inputValue = event.target.value;
    this.setState({inputCode: inputValue});
    this.checkEnableNext();
  }

  checkEnableNext() {
    let inputPhone = this.state.inputPhone;
    let inputCode = this.state.inputCode;
    if (null != inputPhone && inputPhone.length == 11 && null != inputCode && inputCode.length > 2) {
      this.setState({enableNext: true});
    } else {
      this.setState({enableNext: false});
    }
    this.forceUpdate();
  }

  _areaChange(event){
    let selected = event.target.value;
    var source = this._makeArea2(selected);
    this.setState({area1:selected,area2Source:source,area2:source[0].value});
    this.setState({area2:source[0].value});
  }

  _area2Change(event){
    let selected = event.target.value;
    this.setState({area2:selected});
  }

  showToastNow(toast) {
    this.setState({
      toastInfo: toast.text,
      showToast: true,
      toastIcon: toast.icon
    });
    if (toast.duration > 0) {
      this.state.toastTimer = setTimeout(()=> {
        this.setState({showToast: false});
      }, toast.duration);
    }
  }

  positiveConfirm() {
    this.setState({showConfirm: false});
    //上报收不到短信验证码的手机号
    if (this.props.reportPhoneNum !== '') {
      let url = this.props.reportPhoneNum + this.state.inputPhone;
      this.sendRequest = Ajax.POST(url, {data: ''})
      .then((res) => {
        console.log("上报手机号码成功",this.state.inputPhone);//上报成功,什么都不做
      })
      .catch((res) => {
        //上报失败,尝试上报3次
        if (this.state.staticVar.failReportTimes < 3) {
          this.positiveConfirm();
        }
        this.state.staticVar.failReportTimes++;
        console.log("上报手机号码"+this.state.inputPhone+"失败"+this.state.staticVar.failReportTimes+"次");
      })
      .then(() => { //finally

      });
    }
  }

  nagativeConfirm() {
    this.setState({showConfirm: false});
  }

  handleForm(e){
    e.preventDefault();
  }

  render() {
    let btnClass = 'weui_btn weui_btn_primary';
    if (this.state.enableNext) {
      btnClass = 'weui_btn weui_btn_primary';
    } else {
      btnClass = 'weui_btn weui_btn_primary weui_btn_disabled';
    }

    return (
        <Page title="在线申报">
          <Form onSubmit={this.handleForm} >
            <Cell select selectPos="after">
              <CellHeader>
                <Label>地区:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.area1}  onChange={this._areaChange.bind(this)}>
                  {
                    _.map(this.state.area1Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>

              </CellBody>
              <CellBody>
                <Select defaultValue={this.state.area2}  onChange={this._area2Change.bind(this)}>
                  {
                    _.map(this.state.area2Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>
            <Cell vcode={true} warn={true}>
              <CellHeader>
                <Label>图形验证码</Label>
              </CellHeader>
              <CellBody>
                <Input type="string" placeholder="图形验证码" onChange={this.checkImageCodeInput.bind(this)}/>
              </CellBody>
              <CellFooter onClick={this.refresh.bind(this)}>
                <img src={this.state.imageCodeUrl} />
              </CellFooter>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>手机</Label>
              </CellHeader>
              <CellBody>
                <Input type="number" placeholder="请输入手机号"
                       onChange={this.checkNumberInput.bind(this)}/>
              </CellBody>
              <CellFooter>
                <CountDownBtn ref="countDownBtn"
                              enableCountDown={this.state.enableCountDown}
                              beginCount={this.state.beginCount}
                              handleOnClick={this._sendCode.bind(this)}
                              countFinal={()=>{this.setState({beginCount:false})}} />
              </CellFooter>
            </Cell>
            <Cell>
              <CellHeader>
                <Label>验证码</Label>
              </CellHeader>
              <CellBody>
                <Input type="number" placeholder="短信验证码" onChange={this.checkCodeInput.bind(this)}/>
              </CellBody>
              <CellFooter>
              </CellFooter>
            </Cell>
          </Form>
          <ButtonArea>
            <Button onClick={this._bindPhone.bind(this)} className={btnClass}>完成验证</Button>
          </ButtonArea>
          <Toast ref='commonToast' iconSize="small" icon={this.state.toastIcon} show={this.state.showToast}>{this.state.toastInfo}</Toast>
          <Confirm
              show={this.state.showConfirm}
              title={this.state.confirm.title}
              buttons={this.state.confirm.buttons}>
          </Confirm>
        </Page>
    )
  }
}
OperationOL.defaultProps = {
  bindSource: Config.api_host + '/client/home/login?mobile=',
  sendSource: Config.api_host + '/client/home/sendsms',
  reportPhoneNum: Config.api_host +'/client/home/captchaUnreachable?mobile=',
  imageCodeUrl: Config.api_host + '/client/home/getvalidatecode?time=',
};


export default OperationOL;
