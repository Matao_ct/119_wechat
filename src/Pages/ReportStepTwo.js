import React, { Component } from 'react';


import Page from '../components/page';

import WeUI from 'react-weui';
import 'weui';

import {hashHistory} from 'react-router';
const{
    ButtonArea,
    Button,
    CellHeader,
    CellBody,
    Form,
    FormCell,
    Cell,
    Label,
    TextArea,
    CellsTitle,
    Select,
} = WeUI;

var Config = require('./Config');


class ReportStepTwo extends Component {

  constructor(props){
    super(props);
    this._initialState();
  }


  _makeArea1(){
    var area1 = [];
    _.map(Config.area,(value,key) =>{
      var item = {};
      item['value'] = value.id;
      item['label'] = value.name;
      area1.push(item);
    });
    return area1;
  }

  _makeArea2(area1){

    console.log('area1 select',area1);

    var source = _.filter(Config.area,(value,key)=>{
      if( value.id == area1)
        return value;
    })

    var area2 = [];

    _.map(source[0].children,(value,key)=>{
      var item ={};
      item['value'] = value.id;
      item['label'] = value.name;

      area2.push(item);
    })
    return area2;
  }

  _initialState(){

    var area1 = this._makeArea1();


    var area2 = [];
    if(this.props.location.state != undefined)
    {
      area2 = this.props.location.state.area2Source;
    }
    else{
      area2 = this._makeArea2(area1[0].value);
    }
    if(this.props.location.state != undefined)
    {
      this.state ={
        area:this.props.location.state.area,
        area1:this.props.location.state.area1,
        area2:this.props.location.state.area2,
        type:this.props.location.state.type,
        content:this.props.location.state.content,
        name:this.props.location.state.name,
        sID:this.props.location.state.sID,
        tel:this.props.location.state.tel,
        addr:this.props.location.state.addr,
        area1Source:area1,
        area2Source:area2,
      }
    }
    else {
      this.state ={
        area:1,
        area1:area1[0].value,
        area2:area2[0].value,
        type:1,
        content:null,
        name:null,
        sID:null,
        tel:null,
        addr:null,
        area1Source:area1,
        area2Source:area2,
      }
    }
  }

  _typeChange(event){
    let selected = event.target.value;
    this.setState({type:selected});
  }

  _areaChange(event){
    let selected = event.target.value;
    var source = this._makeArea2(selected);
    console.log('area2',source);
    this.setState({area1:selected,area2Source:source,area2:source[0].value});
    this.setState({area2:source[0].value});
    console.log('area2 selected',source[0].value);
  }

  _area2Change(event){
    let selected = event.target.value;
    this.setState({area2:selected});
  }


  _contentChange(event){
    let selected = event.target.value;
    this.setState({content:selected});
  }

  render() {
    return (
        <Page title="投诉建议" backRoute='/reportStep1' >
          <CellsTitle>填写投诉信息</CellsTitle>
          <Form>

            <Cell select selectPos="after">
              <CellHeader>
                <Label>投诉地区:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.area1}  onChange={this._areaChange.bind(this)}>
                  {
                    _.map(this.state.area1Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>

              </CellBody>
              <CellBody>
                <Select defaultValue={this.state.area2}  onChange={this._area2Change.bind(this)}>
                  {
                    _.map(this.state.area2Source, (value, key) => {
                      return (
                          <option value={value.value} key={value.value}>{value.label}</option>
                      )
                    })
                  }
                </Select>
              </CellBody>
            </Cell>


            <Cell select selectPos="after">
              <CellHeader>
                <Label>投诉类别:</Label>
              </CellHeader>
              <CellBody>
                <Select defaultValue={this.state.type} data={[
                  {
                    value: 1,
                    label: '服务态度差'
                  },
                  {
                    value: 2,
                    label: '乱收费'
                  },
                  {
                    value: 3,
                    label: '内部人员违法违纪'
                  }
                ]}  onChange={this._typeChange.bind(this)} />
              </CellBody>
            </Cell>

            <Cell>
              <CellHeader>
                <Label>投诉内容:</Label>
              </CellHeader>
              <CellBody>
                <TextArea placeholder="请输入内容" defaultValue={this.state.content} onChange={this._contentChange.bind(this)} rows="3" maxlength="200" />
              </CellBody>
            </Cell>
          </Form>
          <ButtonArea>
            <Button onClick={()=>{hashHistory.push({pathname:'/reportStep3',state:this.state})}}>下一步</Button>
          </ButtonArea>
        </Page>
    );
  }
}

export default ReportStepTwo;
