
import $ from 'jquery';

const ajax = (url, {
  method='GET',
  data,
  uuid
}) => {
  return new Promise((resolve, reject) => {
    $.ajax(url, {
      method,
      headers: {'X-Requested-With': 'XMLHttpRequest'},
      data
    }).then((res) => {


      if (res.code == 0) {
        resolve(res.data)
      } else {
        reject(res)
        console.warn(res)
      }
    }).fail((res) => {
      reject(res)
    })
  })
}

// xhrFields: {
//   withCredentials: true
// },
// crossDomain: true,

export const GET = (url, options={}) => {
  options.method = 'GET'
  return ajax(url, options)
}
export const PUT = (url, options={}) => {
  options.method = 'PUT' //update
  return ajax(url, options)
}
export const POST = (url, options={}) => {
  options.method = 'POST'
  return ajax(url, options)
}
export const DELETE = (url, options={}) => {
  options.method = 'DELETE'
  return ajax(url, options)
}
