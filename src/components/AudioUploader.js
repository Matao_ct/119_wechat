var Config = require('../Pages/Config');
var Ajax = require('./ajax');

export const AudioUploader = (serverIds) =>{

  function getURL(URL) {
    return new Promise(function (resolve, reject) {

      Ajax.GET(URL,[])
        .then((res) =>{
          // console.log('res is ',res);
          resolve(res);
        })
        .catch((res) =>{
          reject(res.msg);
        });
    });
  }

  var request = [];

  _.map(serverIds,(value,key) => {
    var source = Config.api_host + '/client/upload/getFromwechat?serverId=' + value.serverId;
    // console.log(source);
    request.push(getURL(source));
  });

  // console.log(request);

  function main() {
    return Promise.all(request);
  }

  return main();

}
