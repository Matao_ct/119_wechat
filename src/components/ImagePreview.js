import React,{Component,PropTypes} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import {Label,Panel,Button,ButtonArea,Dialog,Toast} from 'react-weui';

var Config = require('config');

export default class ImagePreview extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    files: PropTypes.array,
  };
  static defaultProps = {
    title: '图片',
    files: [],
  };

  renderFiles() {
    return this.props.files.map((item)=> {
      let cls = classNames({
        weui_uploader_file: true,
      });
      let thumbUrl = item +'?iopcmd=thumbnail&type=4&width=100';
      let fileStyle = {
        backgroundImage: `url(${thumbUrl})`
      };
      return (
        <li className={cls} style={fileStyle} onClick={this.previewImage.bind(this,item)}/>
      );
    });
  }

  previewImage(url){
    Config.wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: this.props.files // 需要预览的图片http链接列表
    });
  }

  render() {
    const {className, title, maxCount, ...others} = this.props;
    const cls = classNames({
      weui_uploader: true,
      [className]: className
    });

    return (
      <div className={cls}>
        <div className="weui_uploader_bd">
          <ul className="weui_uploader_files">
            {this.renderFiles()}
          </ul>
        </div>
      </div>
    );
  }
}
