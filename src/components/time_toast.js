import React, {Component} from 'react';
import {Mask, Icon} from 'react-weui';

class TimeToast extends Component {

  constructor(props) {
    super(props);
    this._initialState();
  }

  _initialState() {
    this.state = {
      show: this.props.show,
      timeout: null,
    };
  }

  componentDidMount() {
    if (this.props.duration) {
      this.setState({
        timeout: setTimeout(()=> {
          this.setState({show: false});
          clearTimeout(this.state.timeout);
          if (this.props.onFinish) this.props.onFinish(this);
        }, this.props.duration)
      });
    }
  }

  componentWillUnmount() {
    clearTimeout(this.state.timeout);
  }


  render() {
    const {icon, children, iconSize} = this.props;

    return (
      <div className={icon === 'loading' ? 'weui_loading_toast' : ''} style={{display: this.state.show ? 'block' : 'none'}}>
        <Mask transparent={true}/>
        <div className="weui_toast">
          <Icon value={icon} size={iconSize}/>
          <p className="weui_toast_content">{children}</p>
        </div>
      </div>
    );
  }
}
TimeToast.propTypes = {
  icon: React.PropTypes.string,
  iconSize: React.PropTypes.string,
  show: React.PropTypes.bool,
  duration: React.PropTypes.number,
  onFinish: React.PropTypes.func,
}

TimeToast.defaultProps = {
  icon: 'toast',
  show: false,
}

export default TimeToast;
