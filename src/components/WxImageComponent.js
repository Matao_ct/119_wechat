import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import {Label,Panel,Button,ButtonArea,Dialog,Toast,ActionSheet} from 'react-weui';
var Config = require('../Pages/Config');

export default class WxUploader extends Component {
  static propTypes = {
    title: PropTypes.string,
    maxCount: PropTypes.number,
    maxWidth: PropTypes.number,
    afeterChange: PropTypes.func,
    onError: PropTypes.func,
    lang: PropTypes.object
  };

  static defaultProps = {
    title: '图片上传',
    maxCount: 3,
    maxWidth: 500,
    afeterChange:undefined,
    onError: ((msg)=> {
      alert(msg);
    }),
    lang: {
      maxError: maxCount => `最多只能上传${maxCount}张图片`
    }
  };

  constructor(props) {
    super(props);
    this._initialState();
  }

  _initialState() {
    this.state = {
      currentCount: 0,
      tempFiles: [],
      showToast:{
        show:false,
        icon:null,
        msg:null,
        timer:null,
      },

      showAction: false,
      actionItem: undefined,
      menus: [{
        label: '查看大图',
        onClick: ()=> {
          this.previewItem();
        }
      }, {
        label: '删除',
        onClick: ()=> {
          this.deleteItem();
        }
      }],
      actions: [
        {
          label: '取消',
          onClick: this.hide.bind(this)
        }
      ]
    };
  }

  chooseImage() {
    var that = this;
    Config.wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var resultLocalId = res.localIds[0]; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
        if (that.state.tempFiles.length >= that.props.maxCount) {
          that.props.onError(lang.maxError(that.props.maxCount));
        } else {
          Config.wx.uploadImage({
              localId: resultLocalId, // 需要上传的图片的本地ID，由chooseImage接口获得
              isShowProgressTips: 1, // 默认为1，显示进度提示
              success: function (res) {
                var resultServerId = res.serverId; // 返回图片的服务器端ID
                let result = {
                  localId: resultLocalId,
                  serverId: resultServerId
                }
                console.log('微信端选择成功' + result.localId);
                console.log('微信端上传成功' + result.serverId);
                let newFiles = that.state.tempFiles;
                if (!newFiles) {
                  newFiles = [];
                }
                newFiles.push(result);
                that.setState({
                  tempFiles: newFiles
                });
                //回调,将当前列表做参数传出
                if(that.props.afeterChange){
                  console.log('调用回调' + newFiles);
                  that.props.afeterChange(newFiles);
                }
              },
              failed: (result) => {
                console.log('微信端上传失败 ' + result);
                that._showToast('图片上传失败', 'loading', 2000);
              }
            }
          );
        }
      },
      failed: (result) => {
        console.log('微信端上传失败 ' + result);
        that._showToast('图片上传失败', 'loading', 2000);
      }
    });
  }

  _showToast(msg,icon,timer) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if(timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show:false}});
      }, timer);
  }

  showAction(item){
    this.setState({actionItem: item});
    this.setState({showAction: true});
  }

  deleteItem(){
    let item =this.state.actionItem;
    let newFiles = [];
    this.state.tempFiles.map((i)=>{
      if(i.localId == item.localId){
        console.log('命中删除计算' + i.localId);
      }
      else {
        newFiles.push(i);
      }
    })
    this.setState({
      tempFiles: newFiles
    });
    //回调,将当前列表做参数传出
    if(this.props.afeterChange){
      this.props.afeterChange(newFiles);
    }
    this.hide();
  }

  previewItem(){
    let urls = [];
    this.state.tempFiles.map((item)=>{
      urls.push(item.localId)
    })

    Config.wx.previewImage({
      current: this.state.actionItem.localId, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    });
    this.hide();
  }


  hide() {
    this.setState({actionItem: undefined});
    this.setState({showAction: false});
  }

  renderFile() {
    return this.state.tempFiles.map((item)=> {
      let cls = classNames({
        weui_uploader_file: true,
      });
      let fileStyle = {
        backgroundImage: `url(${item.localId})`
      };
      return (
        <li className={cls} style={fileStyle} onClick={this.showAction.bind(this,item)}/>
      );
    });
  }


  render() {
    const {className, title, maxCount, ...others} = this.props;
    const cls = classNames({
      weui_uploader: true,
      [className]: className
    });

    return (
      <div className={cls}>
        <div className="weui_uploader_hd weui_cell">
          <div className="weui_cell_bd weui_cell_primary">{title}</div>
          <div className="weui_cell_ft">{this.state.tempFiles.length}/{maxCount}</div>
        </div>
        <div className="weui_uploader_bd">
          <ul className="weui_uploader_files">
            {this.renderFile()}
          </ul>
          <div className="weui_uploader_input_wrp">
            <input
              ref="uploader"//let react to reset after onchange
              className="weui_uploader_input"
              accept="image/jpg,image/jpeg,image/png,image/gif"
              onClick={this.chooseImage.bind(this)}
            />
          </div>
        </div>
        <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>
        <ActionSheet menus={this.state.menus} actions={this.state.actions} show={this.state.showAction} onRequestClose={this.hide.bind(this)} />
      </div>
    );
  }
};
