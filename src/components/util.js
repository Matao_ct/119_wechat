import * as constTypes from './const'

export const const_parse = (type, key) => {
  return _.chain(constTypes)
    .get(type)
    .get(key)
    .value()
}
