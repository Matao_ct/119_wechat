import React,{Component,PropTypes} from 'react';
import classNames from 'classnames';
import {AudioUploader} from '../components/AudioUploader';
import {If,Else,Then} from 'react-if';
import {Label,Panel,Button,ButtonArea,Dialog,Toast,Cells,Cell,CellHeader,CellBody,CellFooter,ActionSheet} from 'react-weui';

const {Alert} = Dialog;

var Config = require('config');

const deleteIcon = Config.static_host + "/wechat/static/imageSource/delete.png";
const playIcon = Config.static_host + "/wechat/static/imageSource/play.png";
const stopIcon = Config.static_host + "/wechat/static/imageSource/stop.png";
const recordIcon = Config.static_host + "/wechat/static/imageSource/recordBtn.png";


export default class AudioComponent extends React.Component{
  static propTypes = {
    title:PropTypes.string,
    maxCount:PropTypes.number,
    finishRecord: PropTypes.func,
    onError: PropTypes.func,
    files: PropTypes.array,
    lang: PropTypes.object
  };
  static defaultProps = {
    title:'录音',
    maxCount : 4,
    files: [],
    finishRecord: undefined,
    onError: undefined,
    lang:{
      maxError: maxCount => `最多只能上传${maxCount}段音频`
    }
  };


  constructor(props){
    super(props);
    this._initialState();
  }


  componentDidMount(){
    this._initWx();
  }

  _initWx(){
    //播放结束的回调
    Config.wx.onVoicePlayEnd({
      success: (res) => {
        var localId = res.localId; // 返回音频的本地ID
        console.log("自动停止 " + localId);
        this.setState({currentPlay:-1});
      }
    });

    //若到达60s,自动完成
    Config.wx.onVoiceRecordEnd({

      complete: (res) => {
        this._hideAlert();
        // this._showToast('音频处理中','loading',-1);
        this._afterStopRecord(res);
      }
    })
  }


  _initialState() {
    this.state = {
      currentCount:0,
      currentPlay:-1,
      audioFiles:[],
      alert: {
        showAlert:false,
        title: '标题标题',
        buttons: [
          {
            label: '好的',
            onClick: this._hideAlert.bind(this)
          }
        ]
      },
      showToast:{
        show:false,
        icon:null,
        msg:null,
        timer:null,
      },
      actionSheet:{
        show: false,
        menus: [{
          label: '删除当前音频',
          onClick: ()=>{

          }
        }],
        actions: [{
          label: '取消',
          onClick: this._hideActionSheet.bind(this)
        }]
      },
      currentDelete:{
        k:'',
        v:''
      }
    };

  }

  _showToast(msg,icon,timer) {
    this.setState({
      showToast:{
        show:true,
        icon:icon,
        msg:msg,
      },
    });

    if(timer >= 0)
      this.state.showToast.timer = setTimeout(()=> {
        this.setState({showToast: {show:false}});
      }, timer);
  }

  _showAlert(title,buttonLabel,recording){
    this.setState({alert:{showAlert: true,title:title, buttons:[{label:buttonLabel,onClick: recording?this._stopRecord.bind(this):this._hideAlert.bind(this) }]}});
  }



  _hideAlert(){
    this.setState({alert:{showAlert: false,title:'', buttons:[{label:'',onClick:this._hideAlert.bind(this)}]}});
  }


  _showActionSheet() {
    this.setState({actionSheet:{show: true}});
  }

  _hideActionSheet() {
    this.setState({actionSheet:{show: false}});
  }


  render(){

    const { className, title, maxCount, finishRecord, ...others } = this.props;
    const cls = classNames({
      weui_uploader: true,
      [className]: className
    });

    return(
      <div >
        <Alert
          show={this.state.alert.showAlert}
          title={this.state.alert.title}
          buttons={this.state.alert.buttons}>
        </Alert>
        <Toast show={this.state.showToast.show} icon={this.state.showToast.icon} iconSize="small">{this.state.showToast.msg}</Toast>

        <div style={styleSheet.countStyle}>
          <div >{title}</div>
          <div >{this.state.currentCount}/{maxCount}</div>
        </div>
        <div>
          <Cells>
            {
              _.map(this.state.audioFiles, (v,k)=> {
{/*debugger;*/}
                if(this.state.currentPlay == k){
                  return(
                    <div>
                      <Cell key={k} >
                        <CellHeader onClick={()=>{this._recordClick(k,v);}}>
                          <img src={stopIcon} alt="" style={styleSheet.playIcon}/>
                        </CellHeader>
                        <CellBody onClick={()=>{this._recordClick(k,v);}} style={styleSheet.audioCell}>
                          音频&nbsp;&nbsp;播放中
                        </CellBody>
                        <CellFooter onClick = {()=>{this._deleteClick(k,v);}}>
                          <img src={deleteIcon} alt="" style={styleSheet.deleteIcon}/>
                        </CellFooter>
                      </Cell>

                    </div>
                  )
                }
                else {
                  return(
                    <div>
                      <Cell key={k} >
                        <CellHeader onClick={()=>{this._recordClick(k,v);}}>
                          <img src={playIcon} alt="" style={styleSheet.playIcon}/>
                        </CellHeader>

                        <CellBody onClick={()=>{this._recordClick(k,v);}} style={styleSheet.audioCell}>
                          音频
                        </CellBody>

                        <CellFooter onClick = {()=>{this._deleteClick(k,v);}}>
                          <img src={deleteIcon} alt="" style={styleSheet.deleteIcon}/>
                        </CellFooter>
                      </Cell>
                    </div>
                  )
                }
              })
            }
          </Cells>
        </div>

        <div>
          <div style={styleSheet.recordButtons}>
            <img src={recordIcon} alt="" style={styleSheet.recordIcon} onClick={this._handleStartRecord.bind(this)}/>
          </div>
        </div>

        <ActionSheet show={this.state.actionSheet.show} menus={
          [{label: '删除当前音频', onClick: ()=>{this._deleteRecord(this.state.currentDelete.k,this.state.currentDelete.v);}},
            {label: '取消',onClick:()=>{this._hideActionSheet();}},
          ]} actions={this.state.actionSheet.actions} onRequestClose={this._hideActionSheet.bind(this)} />

      </div>

    );
  }

  _handleStartRecord()
  {
    const langs = this.props.lang;
    if(this.state.audioFiles.length >= this.props.maxCount){
      this._showAlert(langs.maxError(this.props.maxCount),'确定',false);
      return;
    }

    this._startRecord();
  }


  _startRecord(){
    //开始录音
    console.log('start button click');
    this._showAlert('录音中...','停止录音',true);
    Config.wx.startRecord()
  }

  _afterStopRecord(res){
    console.log('res is => ' + res.localId);

    this._showToast('音频处理中','loading',-1);
    Config.wx.uploadVoice({
      localId: res.localId, // 需要上传的音频的本地ID，由stopRecord接口获得
      isShowProgressTips: 0, // 默认为1，显示进度提示
      success:  (result) => {
        // 返回音频的服务器端ID
        var serverId = result.serverId;
        //构造URL
        var source = Config.api_host + '/teacher/upload/getFromwechat?serverId=' + serverId;
        console.log(res.localId +  ' => 微信端上传完成 => ' + source);

        result.localId = res.localId;
        this.state.audioFiles.push(result);

        console.log('audioFiles => ' + this.state.audioFiles);

        //回调,将当前列表做参数传出
        if(this.props.finishRecord)
          this.props.finishRecord(this.state.audioFiles);

        this._showToast('音频处理中','loading',0);
        //更新state,刷新界面
        this.setState({
          currentCount:this.state.audioFiles.length
        });
      },
      failed:(result) =>{
        console.log('微信端上传失败 ' + result);
        this._showToast('音频处理失败','failed',2000);
      }
    });
  }

  _stopRecord(){
    //停止录音
    console.log('stop button click');
      Config.wx.stopRecord({
        success: (res) => {

          //隐藏录音对话框
          this._hideAlert();
          this._afterStopRecord(res);
        }
      });
  }

  _recordClick(k,v) {

    //取得k 对应的 recordId;
    if(this.state.currentPlay != -1){
      //停止当前正在播放的
      console.log('停止播放 ' + v.localId);
      this._stopPlay(v.localId,this.state.currentPlay);

    }
    if(this.state.currentPlay != k){

      //如果是切换播放,则开始播放新的音频
      //播放指定音频
      console.log('正在播放' + v.localId);
      this._playRecord(v.localId,this.state.currentPlay);

      //标记为正在播放
      this.setState({currentPlay:k});
    }
    else
    { //如果是取消播放,则标记为终止
      this.setState({currentPlay:-1});
    }
  }


  _stopPlay(recordId,k){
    Config.wx.stopVoice({
      localId: recordId
    });
  }


  _playRecord(recordId,k){
    Config.wx.playVoice({
      localId:recordId
    });
  }


  _deleteRecord(k,v){

    console.log('key => '+ k,'value => '+ v);

    this._hideActionSheet();

    //如果当前正在播放,停止当前播放
    if(this.state.currentPlay == k){
      this._stopPlay(v.localId,k);
      this.setState({currentPlay:-1});
    }

    _.remove(this.state.audioFiles,(n)=>{
      return n.localId == v.localId;
    });

    this.setState({audioFiles:this.state.audioFiles,currentCount:this.state.audioFiles.length});

    // console.log(this.state.audioFiles);
  }

  _deleteClick(k,v){

    this.setState({currentDelete:{k:k,v:v}});

    this._showActionSheet();
  }

}

const styleSheet = {
  recordButtons:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
  },

  countStyle:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between'
  },
  reactAudioPlayer:{
    marginTop:5,
  },

  audioCell:{
    color: '#999999',
    fontSize: '14',
  },

  recordIcon:{
    display: 'flex',
    width: 48,
    height: 48,
    marginRight: 5,
  },

  playIcon:{
    display: `block`,
    width: `14px`,
    marginRight: `5px`
  },
  deleteIcon:{
    display: `block`,
    width: `25px`,
    marginRight: `5px`
  }

}
