import React, {Component, PropTypes} from 'react';
import {hashHistory} from 'react-router'

class NavBar extends Component {

  render() {
    const {className, title, backRoute,backFunc, ...others} = this.props;
    return (
      <div className={className} style={stylesheet.navbar} {...others}>
        {this.renderBack(backRoute,backFunc)}
        <span style={stylesheet.label}>{title}</span>
      </div>
    );
  }

  renderBack(backRoute,backFunc) {
    if (backRoute || backFunc) {
      return (
        <div style={stylesheet.backLabel} onClick={()=> {
          if(backRoute){
            hashHistory.push(backRoute)
          }else if(backFunc){
            backFunc();
          }
        }}><span style={stylesheet.bigLabel}>{'<'}</span><span style={stylesheet.smallLabel}>返回</span></div>
      )
    }
  }
}

const stylesheet = {
  navbar: {
    display: 'flex',
    alignItems : 'center',
    justifyContent :'center',
    // position: 'absolute',
    marginTop: '0px',
    marginBottom: '8px',
    width: '100%',
    backgroundColor: '#f7f7fa',
    height:'44px',
  },
  label: {
    color: '#000000',
    fontSize: '1.1em',
  },
  backLabel: {
    position: 'absolute',
    alignItems : 'center',
    justifyContent :'center',
    width:'15%',
    left: '16px',
  },

  bigLabel: {
    fontSize: '1.3em',
    fontType: 'bold',
    color: '#888',
  },
  smallLabel: {
    fontSize: '0.9em',
    color: '#888',
  }
};

export default NavBar;
