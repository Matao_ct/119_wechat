export const complaintsType = {
  2: 'COST',
  3: 'INLLEGAL',
  1: 'ATTITUDE',
}

export const queryStatus ={
  SUBMIT:'已提交',
  DRAFT:"草稿",
  AUDITING:"审核中",
  SUPPLEMENT:"待补件",
  UN_CHOOSE:"未抽中",
  PASS:"合格",
  NO_PASS:"不合格",

}


export const homeType ={
  FINISH_BOOK:'建设工程竣工验收消防备案抽查结果公告',
  DESIGN_BOOK:"建设工程消防设计备案抽查结果公告",
  DESIGN_CONFIRM:"建设工程消防设计审核公告",
  ACCEPT:"建设工程消防验收公告",
  USAGE_PRODUCE:"恢复施工、使用、生产、经营检查公告",
}


export const uploadType ={
  1: 'DESIGN_BOOK',
  2:'FINISH_BOOK:',
  3:'DESIGN_CONFIRM:',
  4:'ACCEPT:',
  5:'USAGE_PRODUCE:',
}

// export const project_types = {
//   1:'NEW',
//   2:'EXTEND',
//   3:'DECORATE',
//   4:'KEEP_TEMP',
//   5:'CHANGE_USAGE'
// }
//
//
// export const usage_type_for_base_info = {
//   1:'A1',
//   2:'A2',
//   3:'A3',
//   4:'A4',
//   5:'A5',
//   6:'A6',
//   7:'A7',
//   8:'A8',
//   9:'A9',
//   10:'A10',
//   11:'A11',
// }
