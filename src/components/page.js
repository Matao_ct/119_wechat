import React, {Component} from 'react';
import NavBar from './navbar';

export default class Page extends Component {
  render() {
    const {title, className,backRoute,backFunc,children} = this.props;
    return (
      <div className={`page ${className || ''}`} style={styleSheets.page}>
        <NavBar title={title} backRoute={backRoute} backFunc={backFunc} />
        <div className={`body`}>
          {children}
        </div>
      </div>
    );
  }
};


const styleSheets ={
  page:{
    // flex:1,
    // flexDirection:'row',
    // backgroundColor:'#f0f0f0',
  }
}