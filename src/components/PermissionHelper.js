var Config = require('config');


export const permissionCheck = (routeName) => {
  let permissionName = permissionDic[routeName];
  return !!_.find(Config.permissions,{'identity':permissionName});
}


const permissionDic = {
  myExchange        : 'WXTmyexchange',
  patrol            : 'WXTpatrolList',
  patrolarrange     : 'WXTarrange',
  patrolrecord      : 'WXTnewrecord',
  attendancerecords : 'WXTattendancelist',
  exchange          : 'WXTexchangeList',
  maintain          : 'WXTmaintainList',
  mymaintain        : 'WXTmyMaintain',
  todolist          : 'WXTtodolist',
  sendApplyCar      : 'WXTsendApplyCar',
  shopping          : 'WXTshopping',
  roomSubscribe     : 'WXTroomsubscribe',
}
